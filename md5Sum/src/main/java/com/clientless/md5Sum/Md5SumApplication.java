package com.clientless.md5Sum;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Md5SumApplication {

	public static void main(String[] args) {
		SpringApplication.run(Md5SumApplication.class, args);
	}

}
