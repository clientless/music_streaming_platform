package com.clientless.md5Sum.service.handler;

import com.clientless.md5Sum.util.Constants;
import com.clientless.md5Sum.repository.AlbumRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Objects;

@AllArgsConstructor
public class IdAlbumHandler {

    private AlbumRepository albumRepository;
    public String handlerId(){
        String format = Constants.PREFIX_ID+ Constants.CODE_FOR_ALBUM;
        if(albumRepository.findAll().isEmpty() || albumRepository.idAlbumLatest().get(0)==null || Objects.equals(albumRepository.idAlbumLatest().get(0), "")){
            return format+1;
        }
        String[] requestToArray = albumRepository.idAlbumLatest().get(0).split(format);
        return format+(Integer.parseInt(requestToArray[requestToArray.length-1])+1);
    }
}
