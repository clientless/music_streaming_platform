package com.clientless.md5Sum.service;

import com.clientless.md5Sum.domain.Album;
import com.clientless.md5Sum.exception.NotFoundException;
import com.clientless.md5Sum.repository.AlbumRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MD5SumComparator {

    @Autowired
    private AlbumRepository albumRepository;

    private byte[] getAlbum1(){
        Optional<Album> album = albumRepository.findAlbumByIdAlbum("MSPALB1");
        if(album.isPresent()){
            System.out.println("Masok");
            return album.get().getCoverAlbum();
        }
        System.out.println("Keluar");

        throw new NotFoundException("id not found");
    }
    private byte[] getAlbum2(){
        Optional<Album> album = albumRepository.findAlbumByIdAlbum("MSPALB5");
        if(album.isPresent()){
            System.out.println("Masok2");
            return album.get().getCoverAlbum();
        }
        System.out.println("Keluar2");
        throw new NotFoundException("id not found");
    }
    public String result(){
        byte[] alb1 = getAlbum1();
        byte[] alb2 = getAlbum2();

//        String file1Path = "C:\\Program Files\\Difinite\\fif\\Object_Storage\\document\\DEV_MUJAHID_OUT1918.sql";
//        String file2Path = "C:\\Program Files\\Difinite\\fif\\Object_Storage\\document\\DEV_MUJAHID_OUT19180.sql";

        String checksum1 = MD5Sum.generateMD5Checksum(alb1);
        String checksum2 = MD5Sum.generateMD5Checksum(alb2);

        if (checksum1 != null && checksum2 != null) {
            if (checksum1.equals(checksum2)) {
                return "File contents are identical. " + checksum1.length() +" "+checksum2.length();
            } else {
                return "File contents are different."+ checksum1.length() +" "+checksum2.length();
            }
        } else {
            return "Failed to generate checksum for one or both files.";
        }
    }
}
