package com.clientless.md5Sum.service.inf;

import com.clientless.md5Sum.dto.AlbumDto;
import com.clientless.md5Sum.projection.album.AlbumUploadResponse;
import com.clientless.md5Sum.projection.album.ListAlbum;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.List;

public interface AlbumService {
    AlbumUploadResponse uploadAlbum(AlbumDto request) throws IOException;
    List<ListAlbum> listAlbum() throws IOException;
    }
