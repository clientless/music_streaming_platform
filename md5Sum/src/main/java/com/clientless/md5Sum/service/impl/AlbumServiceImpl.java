package com.clientless.md5Sum.service.impl;

import com.clientless.md5Sum.domain.Album;
import com.clientless.md5Sum.dto.AlbumData;
import com.clientless.md5Sum.dto.AlbumDto;
import com.clientless.md5Sum.dto.AlbumResponse;
import com.clientless.md5Sum.projection.album.AlbumUploadResponse;
import com.clientless.md5Sum.service.MD5Sum;
import com.clientless.md5Sum.service.handler.IdAlbumHandler;
import com.clientless.md5Sum.service.handler.ReleasedYearHandler;
import com.clientless.md5Sum.projection.album.ListAlbum;
import com.clientless.md5Sum.repository.AlbumRepository;
import com.clientless.md5Sum.service.inf.AlbumService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


//@AllArgsConstructor
@Service
public class AlbumServiceImpl implements AlbumService {

    @Autowired
    private AlbumRepository albumRepository;
    Logger logger = LoggerFactory.getLogger(AlbumServiceImpl.class);

    @Override
    public AlbumUploadResponse uploadAlbum(AlbumDto request) throws IOException {
        Album album = new Album();

        ReleasedYearHandler releasedYearHandler = new ReleasedYearHandler();
        IdAlbumHandler idAlbumHandler = new IdAlbumHandler(albumRepository);
        album.setIdAlbum(idAlbumHandler.handlerId());
        album.setTitleAlbum(request.getTitleAlbum());
        album.setReleasedYear(releasedYearHandler.handlerYear(request.getReleasedYear()));
        album.setCoverAlbumType(request.getFile().getContentType());
        album.setCoverAlbum(request.getFile().getBytes());
        album.setDeleted(false);

        albumRepository.save(album);

        AlbumUploadResponse albumUploadResponse = getAlbumUploadResponse(album);

        return albumUploadResponse;

    }

    private static AlbumUploadResponse getAlbumUploadResponse(Album album) {
        AlbumUploadResponse albumUploadResponse = new AlbumUploadResponse();
        albumUploadResponse.setId(album.getId());
        albumUploadResponse.setIdAlbum(album.getIdAlbum());
        albumUploadResponse.setCoverAlbum(album.getCoverAlbum());
        albumUploadResponse.setTitleAlbum(album.getTitleAlbum());
        albumUploadResponse.setCoverAlbumType(album.getCoverAlbumType());
        return albumUploadResponse;
    }


    @Override
    public List<ListAlbum> listAlbum() throws JsonProcessingException {
        try {
            List<ListAlbum> albumList = new ArrayList<>();
            for(Album s : albumRepository.findAll()){
                if (!s.isDeleted()){
                    ListAlbum listAlbum = new ListAlbum();
                    listAlbum.setIdAlbum(s.getIdAlbum());
                    listAlbum.setTitleAlbum(s.getTitleAlbum());
                    listAlbum.setReleasedYear(s.getReleasedYear());

                    RestTemplate restTemplate = new RestTemplate();

// Tentukan URL yang akan diakses
                    String url = "http://localhost:8081/v1/album/view_album/"+s.getIdAlbum();

                    // Lakukan permintaan GET dan terima responsnya
                    ResponseEntity<AlbumResponse> response = restTemplate.getForEntity(url, AlbumResponse.class);

                    AlbumResponse albumResponse = response.getBody();
                    assert albumResponse != null;
                    AlbumData albumData = albumResponse.getData();

                    String checksum1 = MD5Sum.generateMD5Checksum(s.getCoverAlbum());
                    String checksum2 = MD5Sum.generateMD5Checksum(albumData.getCoverAlbum());

                    if (checksum1 != null && checksum2 != null) {
                        if (checksum1.equals(checksum2)) {
                            logger.info("File contents are identical. " + checksum1.length() +" "+checksum2.length());
                        } else {
                            logger.warn("File contents are different."+ checksum1.length() +" "+checksum2.length());
                        }
                    } else {
                        logger.error("Failed to generate checksum for one or both files.");
                    }


                    albumList.add(listAlbum);
                }
            }

            return albumList;
        }catch (Exception e){
            logger.error(e.getMessage());
            return null;
        }
    }

}
