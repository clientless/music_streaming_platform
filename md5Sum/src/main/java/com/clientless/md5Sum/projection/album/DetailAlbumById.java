package com.clientless.md5Sum.projection.album;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Time;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class DetailAlbumById {
    private String titleAlbum;
    private String artistName;
    private String genre;
    private Time durationAlbum;
    private LocalDate releasedYear;
    private boolean isDeleted;
    private byte[] coverAlbum;

}
