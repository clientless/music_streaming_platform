package com.clientless.md5Sum.projection.album;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Time;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AlbumUploadResponse {
    private Long id;
    private String idAlbum;
    private String titleAlbum;
    private LocalDate releasedYear;
    private Time durationAlbum;
    private String coverAlbumType;
    private byte[] coverAlbum;
    private boolean isDeleted;
    private String genre;
    private String genreCode;
    private String artistName;
    private String idArtist;
}
