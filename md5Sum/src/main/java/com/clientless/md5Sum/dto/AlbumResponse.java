package com.clientless.md5Sum.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

public class AlbumResponse {
    private String message;
    private AlbumData data;

    // getters and setters
}

