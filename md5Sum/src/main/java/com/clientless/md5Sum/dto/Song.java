package com.clientless.md5Sum.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

class Song {
    private String titleSong;
    private String durationTime;

    // getters and setters
}
