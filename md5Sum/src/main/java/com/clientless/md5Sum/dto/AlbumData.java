package com.clientless.md5Sum.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AlbumData {
    private String titleAlbum;
    private String artistName;
    private String genre;
    private String durationAlbum;
    private String releasedYear;
    private List<Song> songs;
    private byte[] coverAlbum;

    // getters and setters
}
