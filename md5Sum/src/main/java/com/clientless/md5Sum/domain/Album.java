package com.clientless.md5Sum.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "album_data")
public class Album {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String idAlbum;
    private String titleAlbum;
    private LocalDate releasedYear;
//    private String genreCode;
    private String coverAlbumType;
    @Lob //Save datatype to type Long
    @Column(name = "cover_album", columnDefinition = "LONGBLOB")
    private byte[] coverAlbum;
    //enhance
    private boolean isDeleted;
}
