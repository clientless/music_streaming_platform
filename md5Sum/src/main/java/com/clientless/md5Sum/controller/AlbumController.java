package com.clientless.md5Sum.controller;

import com.clientless.md5Sum.dto.AlbumDto;
import com.clientless.md5Sum.projection.album.AlbumUploadResponse;
import com.clientless.md5Sum.service.inf.AlbumService;
import com.clientless.md5Sum.projection.album.ListAlbum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;


@RestController
@RequestMapping("/v1/album/demo")
public class AlbumController {
    @Autowired
    private AlbumService albumService;

    @PostMapping(value = "/upload", produces = MediaType.APPLICATION_JSON_VALUE)
    public AlbumUploadResponse uploadAlbum(@ModelAttribute AlbumDto request) throws IOException {
        return albumService.uploadAlbum(request);
    }

    @GetMapping(value = "/list_album",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ListAlbum> listAlbum() throws IOException {
        return albumService.listAlbum();
    }

    }
