package com.clientless.song.checker.similarity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SongCheckerSimilarityApplication {

	public static void main(String[] args) {
		SpringApplication.run(SongCheckerSimilarityApplication.class, args);
	}

}
