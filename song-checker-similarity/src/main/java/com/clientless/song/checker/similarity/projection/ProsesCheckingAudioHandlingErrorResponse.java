package com.clientless.song.checker.similarity.projection;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProsesCheckingAudioHandlingErrorResponse {
    @JsonProperty("ERROR_CODE")
    private String ERROR_CODE;

    @JsonProperty("ID_COMPARATOR")
    private String ID_COMPARATOR;

    @JsonProperty("ID_COMPARE")
    private String ID_COMPARE;

    // Add getters and setters as needed

    @Override
    public String toString() {
        return "ProsesCheckingAudioHandlingErrorResponse{" +
                "ERROR_CODE='" + ERROR_CODE + '\'' +
                ", ID_COMPARATOR='" + ID_COMPARATOR + '\'' +
                ", ID_COMPARE='" + ID_COMPARE + '\'' +
                '}';
    }
}
