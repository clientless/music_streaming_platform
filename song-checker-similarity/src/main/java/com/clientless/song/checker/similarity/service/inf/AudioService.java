package com.clientless.song.checker.similarity.service.inf;

import com.clientless.song.checker.similarity.model.Response;
import com.clientless.song.checker.similarity.projection.ProsesCheckingAudioResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;

public interface AudioService {
    ResponseEntity<Response> resultCheckSimilarity(String idSongCompare, String idSongComparator) throws URISyntaxException, JsonProcessingException;


}
