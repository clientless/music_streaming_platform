package com.clientless.song.checker.similarity.exception;

public class NotFoundException extends RuntimeException{

    public NotFoundException(String request) {
        super(request + " not found");
    }
}
