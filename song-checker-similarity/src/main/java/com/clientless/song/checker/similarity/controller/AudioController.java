package com.clientless.song.checker.similarity.controller;


import com.clientless.song.checker.similarity.model.Response;
import com.clientless.song.checker.similarity.projection.ProsesCheckingAudioResponse;
import com.clientless.song.checker.similarity.service.inf.AudioService;
import com.clientless.song.checker.similarity.util.Url;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
@CrossOrigin(origins="http://localhost:8081")
@RestController
@RequestMapping("/v1/msp/scs")
public class AudioController {
    @Autowired
            private AudioService audioService;

    Logger logger = LoggerFactory.getLogger(AudioController.class);
    @GetMapping("/processAudio")
    public ResponseEntity<Response> processCheckingAudio(
            @RequestParam String idSongCompare,
            @RequestParam String idSongComparator
    ) throws URISyntaxException, JsonProcessingException {
        return audioService.resultCheckSimilarity(idSongCompare,idSongComparator);
    }

}
