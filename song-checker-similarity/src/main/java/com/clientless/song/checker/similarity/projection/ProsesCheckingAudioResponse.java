package com.clientless.song.checker.similarity.projection;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ProsesCheckingAudioResponse {
    private String status;
    private String value;
}
