package com.clientless.song.checker.similarity.service.impl;

import com.clientless.song.checker.similarity.model.Response;
import com.clientless.song.checker.similarity.projection.ProsesCheckingAudioHandlingErrorResponse;
import com.clientless.song.checker.similarity.projection.ProsesCheckingAudioResponse;
import com.clientless.song.checker.similarity.service.inf.AudioService;
import com.clientless.song.checker.similarity.util.GenerateResponse;
import com.clientless.song.checker.similarity.util.Url;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Objects;

@Service
public class AudioServiceImpl implements AudioService {
    Logger logger = LoggerFactory.getLogger(AudioServiceImpl.class);
    @Override
    public ResponseEntity<Response> resultCheckSimilarity(String idSongCompare, String idSongComparator) throws URISyntaxException, JsonProcessingException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("idSongCompare", idSongCompare);
        body.add("idSongComparator", idSongComparator);

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

        Url url = new Url();

        String pythonServiceUrl = url.getBase_url_hit_flask_api()+"/process_audio";
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.exchange(
                new URI(pythonServiceUrl),
                HttpMethod.POST,
                requestEntity,
                String.class
        );

        String resultCheckerFromApiFlask = Objects.requireNonNull(responseEntity.getBody()).replace("\n","");
        if(handlingErrorResponse(resultCheckerFromApiFlask)==null){
            ProsesCheckingAudioResponse finalResult = finalResultChecking(resultCheckerFromApiFlask);
            return GenerateResponse.success("Success to Check Similarity Song",finalResult);
        }else {
            return handlingErrorResponse(resultCheckerFromApiFlask);
        }
    }

    private ProsesCheckingAudioResponse finalResultChecking(String resultCheckingFromApiFlask){
        double resultValueToDouble = Double.parseDouble(resultCheckingFromApiFlask);
        String chooseCategorize = categorizeResult(resultValueToDouble);
        return new ProsesCheckingAudioResponse(chooseCategorize,resultCheckingFromApiFlask+"%");
    }

    private String categorizeResult(double resultValueToDouble){
        if(resultValueToDouble >= 0 && resultValueToDouble <5){
            return "SO DIFFERENT";
        } else if (resultValueToDouble>=5 && resultValueToDouble <20) {
            return "DIFFERENT";
        } else if (resultValueToDouble>=20 && resultValueToDouble <35) {
            return "ALMOST SIMILAR";
        } else if (resultValueToDouble>=35 && resultValueToDouble <50){
            return "SIMILAR";
        } else if (resultValueToDouble >=50 && resultValueToDouble <=100){
            return "SO SIMILAR";
        }
        return "UNCATEGORIZED";
    }

    private ResponseEntity<Response> handlingErrorResponse(String resultCheckerFromApiFlask) throws JsonProcessingException {
        ProsesCheckingAudioHandlingErrorResponse prosesCheckingAudioHandlingErrorResponse = mapperHandlingErrorResponse(resultCheckerFromApiFlask);
        if(prosesCheckingAudioHandlingErrorResponse.getERROR_CODE()!=null){
            return switchCaseErrCode(prosesCheckingAudioHandlingErrorResponse);
        }
        return null;
    }

    private ProsesCheckingAudioHandlingErrorResponse mapperHandlingErrorResponse(String resultCheckerFromApiFlask) {
        ObjectMapper objectMapper = new ObjectMapper();
        ProsesCheckingAudioHandlingErrorResponse prosesCheckingAudioHandlingErrorResponse = null;
        try {
            prosesCheckingAudioHandlingErrorResponse = objectMapper.readValue(resultCheckerFromApiFlask, ProsesCheckingAudioHandlingErrorResponse.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return prosesCheckingAudioHandlingErrorResponse != null ? prosesCheckingAudioHandlingErrorResponse : new ProsesCheckingAudioHandlingErrorResponse();
    }

    private ResponseEntity<Response> switchCaseErrCode(ProsesCheckingAudioHandlingErrorResponse prosesCheckingAudioHandlingErrorResponse) throws JsonProcessingException {
        switch (prosesCheckingAudioHandlingErrorResponse.getERROR_CODE()) {
            case "ERR004":
                logger.error("Error: Exceeded retry limit for audio processing");
                return GenerateResponse.error("Error: Exceeded retry limit for audio processing", prosesCheckingAudioHandlingErrorResponse);
            case "ERR003.CO":
                logger.error("Error: ID Song Compare Not Found In DB");
                return GenerateResponse.badRequest("Error: ID Song Compare Not Found In DB", prosesCheckingAudioHandlingErrorResponse);
            case "ERR003.COR":
                logger.error("Error: ID Song Comparator Not Found In DB");
                return GenerateResponse.badRequest("Error: ID Song Comparator Not Found In DB", prosesCheckingAudioHandlingErrorResponse);
            case "ERR003.CO.COR":
                logger.error("Error: ID Song Compare and Comparator Not Found In DB");
                return GenerateResponse.badRequest("Error: ID Song Compare and Comparator Not Found In DB", prosesCheckingAudioHandlingErrorResponse);
            default:
                logger.error("Error: RUN TIME ERROR");
                return GenerateResponse.error("Error: RUN TIME ERROR", prosesCheckingAudioHandlingErrorResponse);
        }
    }

}
