package com.clientless.blacklist.check.service.impl;

import com.clientless.blacklist.check.domain.SongBlacklist;
import com.clientless.blacklist.check.dto.SongBlacklistDto;
import com.clientless.blacklist.check.model.Response;
import com.clientless.blacklist.check.repository.SongBlacklistRepository;
import com.clientless.blacklist.check.service.handler.IdSongBlacklistHandler;
import com.clientless.blacklist.check.service.inf.SongBlacklistService;
import com.clientless.blacklist.check.util.GenerateResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SongBlacklistServiceImpl implements SongBlacklistService {
    @Autowired
    private SongBlacklistRepository songBlacklistRepository;
    Logger logger = LoggerFactory.getLogger(SongBlacklistServiceImpl.class);

    @Override
    public ResponseEntity<Response> songBlacklist(SongBlacklistDto songBlacklistDto) throws JsonProcessingException {
        Optional<SongBlacklist> checkSongInBlacklist = songBlacklistRepository.findSongBlacklistByIdSong(songBlacklistDto.getIdSong());
        if (checkSongInBlacklist.isPresent()){
            SongBlacklist songBlacklist = getSongBlacklistUpdated(songBlacklistDto, checkSongInBlacklist.get());
            songBlacklistRepository.save(songBlacklist);
            return GenerateResponse.success("Updated Data Blacklist", songBlacklist);
        }

        SongBlacklist songBlacklist = getSongBlacklistFirstTime(songBlacklistDto, songBlacklistRepository);
        songBlacklistRepository.save(songBlacklist);
        return GenerateResponse.created("Blacklist", songBlacklist);
    }

    private static String idSongBlacklistHandler(SongBlacklistRepository songBlacklistRepository){
        IdSongBlacklistHandler idSongBlacklistHandler = new IdSongBlacklistHandler(songBlacklistRepository);
        return idSongBlacklistHandler.handlerId();
    }

    private static SongBlacklist getSongBlacklistFirstTime(SongBlacklistDto songBlacklistDto, SongBlacklistRepository songBlacklistRepository) {
        SongBlacklist songBlacklist = new SongBlacklist();
        songBlacklist.setIdSongBlacklist(idSongBlacklistHandler(songBlacklistRepository));
        songBlacklist.setIdSong(songBlacklistDto.getIdSong());
        songBlacklist.setBlacklist(songBlacklistDto.isBlacklist());
        songBlacklist.setBlacklistPermanent(songBlacklistDto.isBlacklistPermanent());
        songBlacklist.setCountWasBlacklist(0L);
        songBlacklist.setStartBlacklistDate(songBlacklistDto.getStartBlacklistDate());
        songBlacklist.setEndBlacklistDate(songBlacklistDto.getEndBlacklistDate());
        songBlacklist.setReasonBlacklist(songBlacklistDto.getReasonBlacklist());
        return songBlacklist;
    }

    private static SongBlacklist getSongBlacklistUpdated(SongBlacklistDto songBlacklistDto, SongBlacklist checkSongInBlacklist) {
        checkSongInBlacklist.setBlacklist(songBlacklistDto.isBlacklist());
        checkSongInBlacklist.setBlacklistPermanent(songBlacklistDto.isBlacklistPermanent());
        checkSongInBlacklist.setCountWasBlacklist(checkSongInBlacklist.getCountWasBlacklist()+1L);
        checkSongInBlacklist.setStartBlacklistDate(songBlacklistDto.getStartBlacklistDate());
        checkSongInBlacklist.setEndBlacklistDate(songBlacklistDto.getEndBlacklistDate());
        checkSongInBlacklist.setReasonBlacklist(songBlacklistDto.getReasonBlacklist());
        return checkSongInBlacklist;
    }
}
