package com.clientless.blacklist.check.util;

public class Constants {
    public static final String PREFIX_ID = "MSP";
    public static final String SUFFIX_ID = "BL";
    public static final String CODE_FOR_SONG = "SONG";
    public static final String CODE_FOR_ARTIST = "ART";
    public static final String CODE_FOR_ALBUM = "ALB";
    public static final String CODE_FOR_GENRE = "GD";
}
