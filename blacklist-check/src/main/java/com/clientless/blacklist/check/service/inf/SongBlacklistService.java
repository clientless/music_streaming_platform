package com.clientless.blacklist.check.service.inf;

import com.clientless.blacklist.check.dto.SongBlacklistDto;
import com.clientless.blacklist.check.dto.SongTakeOutBlacklistDto;
import com.clientless.blacklist.check.model.Response;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;

public interface SongBlacklistService {
    ResponseEntity<Response> songBlacklist(SongBlacklistDto songBlacklistDto) throws URISyntaxException, JsonProcessingException;

}
