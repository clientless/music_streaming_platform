package com.clientless.blacklist.check.service.impl;

import com.clientless.blacklist.check.domain.SongBlacklist;
import com.clientless.blacklist.check.dto.SongBlacklistDto;
import com.clientless.blacklist.check.dto.SongTakeOutBlacklistDto;
import com.clientless.blacklist.check.exception.NotFoundException;
import com.clientless.blacklist.check.model.Response;
import com.clientless.blacklist.check.repository.SongBlacklistRepository;
import com.clientless.blacklist.check.service.handler.IdSongBlacklistHandler;
import com.clientless.blacklist.check.service.inf.SongBlacklistService;
import com.clientless.blacklist.check.service.inf.SongTakeOutBlacklistService;
import com.clientless.blacklist.check.util.GenerateResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.net.URISyntaxException;
import java.time.Instant;
import java.util.Date;
import java.util.Optional;

@Service
public class SongTakeOutBlacklistServiceImpl implements SongTakeOutBlacklistService {
    @Autowired
    private SongBlacklistRepository songBlacklistRepository;
    Logger logger = LoggerFactory.getLogger(SongTakeOutBlacklistServiceImpl.class);

    @Override
    public ResponseEntity<Response> songBlacklistTakeOut(SongTakeOutBlacklistDto songTakeOutBlacklistDto) throws URISyntaxException, JsonProcessingException {
        Optional<SongBlacklist> checkSongInBlacklist = songBlacklistRepository.findSongBlacklistByIdSong(songTakeOutBlacklistDto.getIdSong());
        if (checkSongInBlacklist.isPresent() && !songTakeOutBlacklistDto.isBlacklistPermanent()){
            SongBlacklist songBlacklist = getSongTakeOutBlacklistUpdated(songTakeOutBlacklistDto, checkSongInBlacklist.get());
            songBlacklistRepository.save(songBlacklist);
            return GenerateResponse.success("Updated Data Blacklist", songBlacklist);
        }
        throw new NotFoundException("Not found data: "+songTakeOutBlacklistDto.getIdSong());
    }

    private static SongBlacklist getSongTakeOutBlacklistUpdated(SongTakeOutBlacklistDto songTakeOutBlacklistDto, SongBlacklist checkSongInBlacklist) {
        checkSongInBlacklist.setBlacklist(songTakeOutBlacklistDto.isBlacklist());
        checkSongInBlacklist.setStartBlacklistDate(songTakeOutBlacklistDto.getStartBlacklistDate());
        checkSongInBlacklist.setEndBlacklistDate(Date.from(Instant.now()));
        checkSongInBlacklist.setReasonBlacklist(songTakeOutBlacklistDto.getReasonTakeOutBlacklist());
        return checkSongInBlacklist;
    }

}
