package com.clientless.blacklist.check.config;

public class AuthenticationConfigConstants {
    public static final String SECRET = "Msp_Climis";
    public static final long EXPIRATION_TIME = 300000 ; // 5 min
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
}
