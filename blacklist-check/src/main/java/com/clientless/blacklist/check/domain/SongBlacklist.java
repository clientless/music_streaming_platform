package com.clientless.blacklist.check.domain;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "song_blacklist")
public class SongBlacklist {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String idSongBlacklist;
    private String idSong;
    private boolean isBlacklist;
    private boolean isBlacklistPermanent;
    private Long countWasBlacklist;
    private Date startBlacklistDate;
    private Date endBlacklistDate;
    @Column(length = 10000)
    private String reasonBlacklist;
    @Column(length = 10000)
    private String reasonTakeOutBlacklist;
}
