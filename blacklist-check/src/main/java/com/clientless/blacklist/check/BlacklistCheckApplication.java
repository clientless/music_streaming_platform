package com.clientless.blacklist.check;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlacklistCheckApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlacklistCheckApplication.class, args);
	}

}
