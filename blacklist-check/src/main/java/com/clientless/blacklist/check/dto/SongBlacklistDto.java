package com.clientless.blacklist.check.dto;

import lombok.*;

import java.util.Date;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SongBlacklistDto {
    private String idSong;
    private boolean isBlacklist;
    private boolean isBlacklistPermanent;
    private Date startBlacklistDate;
    private Date endBlacklistDate;
    private String reasonBlacklist;
}
