package com.clientless.blacklist.check.controller;


import com.clientless.blacklist.check.dto.SongBlacklistDto;
import com.clientless.blacklist.check.dto.SongTakeOutBlacklistDto;
import com.clientless.blacklist.check.model.Response;
import com.clientless.blacklist.check.projection.ProsesCheckingAudioResponse;
import com.clientless.blacklist.check.service.inf.SongBlacklistService;
import com.clientless.blacklist.check.service.inf.SongTakeOutBlacklistService;
import com.clientless.blacklist.check.util.Url;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
@CrossOrigin(origins="http://localhost:8084")
@RestController
@RequestMapping("/v1/msp/sb")
public class SongBlacklistController {
    @Autowired
            private SongBlacklistService songBlacklistService;
    @Autowired
    private SongTakeOutBlacklistService songTakeOutBlacklistService;

    Logger logger = LoggerFactory.getLogger(SongBlacklistController.class);
    @PostMapping("/blacklist-song")
    public ResponseEntity<Response> songBlacklist(@RequestBody SongBlacklistDto songBlacklistDto) throws URISyntaxException, JsonProcessingException {
        return songBlacklistService.songBlacklist(songBlacklistDto);
    }


    @PostMapping("/blacklist-song/take-out")
    public ResponseEntity<Response> songBlacklistTakeOut(@RequestBody SongTakeOutBlacklistDto songTakeOutBlacklistDto) throws URISyntaxException, JsonProcessingException {
        return songTakeOutBlacklistService.songBlacklistTakeOut(songTakeOutBlacklistDto);
    }

}
