package com.clientless.blacklist.check.service.handler;

import com.clientless.blacklist.check.repository.SongBlacklistRepository;
import lombok.AllArgsConstructor;

import java.util.Objects;

import static com.clientless.blacklist.check.util.Constants.*;

@AllArgsConstructor
public class IdSongBlacklistHandler {
    private SongBlacklistRepository songBlacklistRepository;
    public String handlerId(){
        String format = PREFIX_ID+CODE_FOR_SONG+SUFFIX_ID;
        if(songBlacklistRepository.idSongBlacklistLatest().isEmpty() ||songBlacklistRepository.idSongBlacklistLatest().get(0)==null || Objects.equals(songBlacklistRepository.idSongBlacklistLatest().get(0), "")){
            return format+1;
        }
        String[] requestToArray = songBlacklistRepository.idSongBlacklistLatest().get(0).split(format);
        return format+ (Integer.parseInt(requestToArray[requestToArray.length-1])+1);
    }
}
