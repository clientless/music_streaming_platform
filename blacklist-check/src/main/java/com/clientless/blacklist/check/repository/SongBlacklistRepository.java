package com.clientless.blacklist.check.repository;

import com.clientless.blacklist.check.domain.SongBlacklist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SongBlacklistRepository extends JpaRepository<SongBlacklist, Long> {
    Optional<SongBlacklist> findSongBlacklistByIdSong(String idSong);
    @Query("SELECT idSongBlacklist FROM SongBlacklist order by idSongBlacklist desc")
    List<String> idSongBlacklistLatest();

}
