package com.clientless.blacklist.check.service.inf;

import com.clientless.blacklist.check.dto.SongBlacklistDto;
import com.clientless.blacklist.check.dto.SongTakeOutBlacklistDto;
import com.clientless.blacklist.check.model.Response;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.ResponseEntity;

import java.net.URISyntaxException;

public interface SongTakeOutBlacklistService {
    ResponseEntity<Response> songBlacklistTakeOut(SongTakeOutBlacklistDto songTakeOutBlacklistDto) throws URISyntaxException, JsonProcessingException;


}
