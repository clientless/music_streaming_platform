//package com.clientless.songmanagement.service.inf;
//
//// MusicSimilarityProject/src/main/java/com/example/AudioComparison.java
//
//import py4j.GatewayServer;
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//
//public class AudioComparison {
//
//    public static void main(String[] args) {
//        GatewayServer gatewayServer = new GatewayServer(new AudioComparison());
//        gatewayServer.start();
//        System.out.println("Gateway Server Started");
//    }
//
//    public String extractFeatures(String filePath, double targetDuration) {
//        try {
//            String command = "python ../../../../../../../../../testPy/audio_features.py " + filePath + " " + targetDuration;
//            Process process = Runtime.getRuntime().exec(command);
//
//            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
//            StringBuilder output = new StringBuilder();
//            String line;
//
//            while ((line = reader.readLine()) != null) {
//                output.append(line).append("\n");
//            }
//
//            int exitCode = process.waitFor();
//            if (exitCode == 0) {
//                return output.toString();
//            } else {
//                return "Error during feature extraction";
//            }
//        } catch (IOException | InterruptedException e) {
//            e.printStackTrace();
//            return "Error during feature extraction";
//        }
//    }
//}
