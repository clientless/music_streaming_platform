//package com.clientless.songmanagement.service.inf;
//
//// MusicSimilarityProject/src/main/java/com/example/SimilarityComparison.java
//
//
//import org.apache.commons.lang3.ArrayUtils;
//import org.apache.commons.math3.linear.Array2DRowRealMatrix;
//import org.apache.commons.math3.linear.RealMatrix;
//import org.apache.commons.math3.linear.SingularValueDecomposition;
//
//public class SimilarityComparison {
//
//    public static void main(String[] args) {
//        AudioComparison audioComparison = new AudioComparison();
//
//        String pathSong1 = "C:\\Program Files\\private\\Backup_From_Ubuntu_pt1\\Home\\Backup_Lukito\\BackUp\\quadtrikal\\Album Raw_Utopia\\Lagu\\Mp3\\Bintang_Jatuh.mp3";
//        String pathSong2 = "C:\\Program Files\\private\\Backup_From_Ubuntu_pt1\\Home\\Backup_Lukito\\BackUp\\quadtrikal\\Album Raw_Utopia\\Lagu\\Mp3\\Bintang_Jatuh.mp3";
//
//        double targetDuration = Math.max(getDuration(pathSong1), getDuration(pathSong2));
//
//        String featuresSong1 = audioComparison.extractFeatures(pathSong1, targetDuration);
//        String featuresSong2 = audioComparison.extractFeatures(pathSong2, targetDuration);
//
//        // Convert string representations of features to double arrays
//        double[][] arrayFeaturesSong1 = convertStringToArray(featuresSong1);
//        double[][] arrayFeaturesSong2 = convertStringToArray(featuresSong2);
//
//        // Perform similarity calculation (example: using cosine similarity)
//        double similarity = calculateCosineSimilarity(arrayFeaturesSong1, arrayFeaturesSong2);
//
//        System.out.println("Similarity between the songs: " + similarity);
//    }
//
//    private static double getDuration(String filePath) {
//        // TODO: Implement logic to get the duration of the audio file
//        return 0.0;
//    }
//
//    private static double[][] convertStringToArray(String features) {
//        String[] rows = features.trim().split("\n");
//        double[][] array = new double[rows.length][];
//
//        for (int i = 0; i < rows.length; i++) {
//            String[] cols = rows[i].trim().split("\\s+");
//            array[i] = new double[cols.length];
//
//            for (int j = 0; j < cols.length; j++) {
//                array[i][j] = Double.parseDouble(cols[j]);
//            }
//        }
//
//        return array;
//    }
//
//    private static double calculateCosineSimilarity(double[][] feature1, double[][] feature2) {
//        RealMatrix matrix1 = new Array2DRowRealMatrix(feature1);
//        RealMatrix matrix2 = new Array2DRowRealMatrix(feature2);
//
//        RealMatrix transpose1 = matrix1.transpose();
//        RealMatrix transpose2 = matrix2.transpose();
//
//        RealMatrix product = transpose1.multiply(matrix2);
//        double norm1 = Math.sqrt(transpose1.multiply(matrix1).getTrace());
//        double norm2 = Math.sqrt(transpose2.multiply(matrix2).getTrace());
//
//        return product.getTrace() / (norm1 * norm2);
//    }
//}
