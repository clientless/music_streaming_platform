package com.clientless.songmanagement.service.impl.waitingapproval;

import com.clientless.songmanagement.domain.mainfeature.Album;
import com.clientless.songmanagement.domain.mainfeature.Song;
import com.clientless.songmanagement.domain.waitingapproval.WaitAlbum;
import com.clientless.songmanagement.domain.waitingapproval.WaitSong;
import com.clientless.songmanagement.dto.DeleteDto;
import com.clientless.songmanagement.dto.waitingapproval.song.WaitSongDto;
import com.clientless.songmanagement.dto.waitingapproval.song.WaitSongEditDto;
import com.clientless.songmanagement.dto.waitingapproval.song.WaitSongStatusApproveDto;
import com.clientless.songmanagement.exception.NotFoundException;
import com.clientless.songmanagement.model.Response;
import com.clientless.songmanagement.projection.waitingapproval.song.*;
import com.clientless.songmanagement.repository.mainfeature.AlbumRepository;
import com.clientless.songmanagement.repository.mainfeature.SongRepository;
import com.clientless.songmanagement.repository.waitingapproval.WaitAlbumRepository;
import com.clientless.songmanagement.repository.waitingapproval.WaitSongRepository;
import com.clientless.songmanagement.service.handler.waitingapproval.song.IdWaitSongHandler;
import com.clientless.songmanagement.service.handler.waitingapproval.song.WaitDurationSongHandler;
import com.clientless.songmanagement.service.handler.waitingapprovalinit.song.InitWaitDurationSongHandler;
import com.clientless.songmanagement.service.inf.waitingapproval.WaitSongService;
import com.clientless.songmanagement.util.GenerateResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;
import java.sql.Time;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;


@Service
@Transactional
public class WaitSongServiceImpl implements WaitSongService {

    private final WaitSongRepository waitSongRepository;
    private final WaitAlbumRepository waitAlbumRepository;
    private final AlbumRepository albumRepository;
    private final SongRepository songRepository;

    public WaitSongServiceImpl(WaitSongRepository waitSongRepository, WaitAlbumRepository waitAlbumRepository, AlbumRepository albumRepository, SongRepository songRepository) {
        this.waitSongRepository = waitSongRepository;
        this.waitAlbumRepository = waitAlbumRepository;
        this.albumRepository = albumRepository;
        this.songRepository = songRepository;
    }


    @Override
    public ResponseEntity<Response> upload(WaitSongDto songDto) throws IOException {
        WaitSong waitSong = new WaitSong();
        IdWaitSongHandler idWaitSongHandler = new IdWaitSongHandler(waitSongRepository);
        WaitDurationSongHandler waitDurationSongHandler = new WaitDurationSongHandler();

        waitSong.setIdSong(idWaitSongHandler.handlerId());
        waitSong.setLanguage(songDto.getLanguage());
        waitSong.setExplicit(Boolean.valueOf(songDto.getExplicit()));
        waitSong.setCreatedBy(songDto.getCreatedBy());
        waitSong.setTitleSong(songDto.getSong().getOriginalFilename());
        waitSong.setSong(songDto.getSong().getBytes());
        waitSong.setSongType(songDto.getSong().getContentType());
        waitSong.setDeleted(false);
        waitSong.setApproved(false);
        waitSong.setCreatedBy(songDto.getCreatedBy());
        waitSong.setCreatedAt(LocalDate.now());
        waitSong.setUpdatedAt(waitSong.getCreatedAt());
        Optional<WaitAlbum> check = waitAlbumRepository.findWaitAlbumByIdAlbum(songDto.getIdAlbum());
        if(check.isPresent()){
            WaitAlbum waitAlbum = check.get();
            waitSong.setIdAlbumMappingForSong(waitAlbum);
        }else {
            throw new NotFoundException("Album with ID Album "+ songDto.getIdAlbum()+ " Not Found");
        }


        //Duration Harus di set paling akhir !!!
        waitSong.setDurationSong(Time.valueOf(waitDurationSongHandler.uploadAudio(songDto.getSong())));


        waitSongRepository.save(waitSong);

        SongWaitUploadResponse songWaitUploadResponse = getSongWaitUploadResponse(waitSong);
        songWaitUploadResponse.setArtistName(waitSong.getIdAlbumMappingForSong().getIdArtistMappingForAlbum().getArtistName());
        songWaitUploadResponse.setTitleAlbum(waitSong.getIdAlbumMappingForSong().getTitleAlbum());
        songWaitUploadResponse.setReleasedYear(waitSong.getIdAlbumMappingForSong().getReleasedYear());


        return GenerateResponse.created("Success to upload", songWaitUploadResponse);
    }

    private static SongWaitUploadResponse getSongWaitUploadResponse(WaitSong waitSong) {
        SongWaitUploadResponse songWaitUploadResponse = new SongWaitUploadResponse();
        songWaitUploadResponse.setSong(waitSong.getSong());
        songWaitUploadResponse.setIdSong(waitSong.getIdSong());
        songWaitUploadResponse.setDurationSong(waitSong.getDurationSong());
        songWaitUploadResponse.setSongType(waitSong.getSongType());
        songWaitUploadResponse.setTitleSong(waitSong.getTitleSong());
        songWaitUploadResponse.setApproved(waitSong.isApproved());
        songWaitUploadResponse.setDeleted(waitSong.isDeleted());
        songWaitUploadResponse.setMadeBy(waitSong.getMadeBy());
        songWaitUploadResponse.setExplicit(waitSong.getExplicit());
        songWaitUploadResponse.setLanguage(waitSong.getLanguage());
        return songWaitUploadResponse;
    }

    @Override
    public byte[] getWaitSongById(String idSong) {
        Optional<WaitSong> optionalWaitSong = waitSongRepository.findWaitSongByIdSong(idSong);
        WaitSong waitSong = optionalWaitSong.orElseThrow(() -> new IllegalArgumentException("Song not found"));// Lakukan operasi lain yang memanipulasi audio jika diperlukan
        return waitSong.getSong();
    }

    @Override
    public ResponseEntity<Response> listSong(String idAlbum) throws IOException {
        List<ListWaitSong> listWaitSongs = new ArrayList<>();
        for(WaitSong waitSong:waitSongRepository.findAllByIdAlbum(idAlbum)){
            ListWaitSong listWaitSong = new ListWaitSong();
            listWaitSong.setIdSong(waitSong.getIdSong());
            listWaitSong.setTitleSong(waitSong.getTitleSong());
            listWaitSong.setArtistName(waitSong.getIdAlbumMappingForSong().getIdArtistMappingForAlbum().getArtistName());
            listWaitSong.setAlbumName(waitSong.getIdAlbumMappingForSong().getTitleAlbum());
            listWaitSong.setYear(String.valueOf(waitSong.getIdAlbumMappingForSong().getReleasedYear()));
            listWaitSong.setGenreType(waitSong.getIdAlbumMappingForSong().getGenreCodeMappingForAlbum().getGenreType());
            listWaitSong.setDurationTime(waitSong.getDurationSong());
            listWaitSongs.add(listWaitSong);
        }
        return GenerateResponse.success("Success to get List Data", listWaitSongs);
    }

    @Override
    public ResponseEntity<Response> getDetailWaitSongById(String idSong) throws JsonProcessingException {
        Optional<WaitSong> check = waitSongRepository.findWaitSongByIdSong(idSong);
        DetailWaitSongById detailWaitSongById = new DetailWaitSongById();
        if(check.isPresent()){
            WaitSong waitSong = check.get();
            detailWaitSongById.setIdSong(waitSong.getIdSong());
            detailWaitSongById.setTitleSong(waitSong.getTitleSong().replaceAll(".mp3",""));
            detailWaitSongById.setDurationSong(waitSong.getDurationSong());
            detailWaitSongById.setExplicit(waitSong.getExplicit());
            detailWaitSongById.setLanguage(waitSong.getLanguage());
            detailWaitSongById.setSongType(waitSong.getSongType());
            detailWaitSongById.setMadeBy(waitSong.getMadeBy());
            detailWaitSongById.setDeleted(waitSong.isDeleted());
            detailWaitSongById.setApproved(waitSong.isApproved());
            detailWaitSongById.setReleasedYear(waitSong.getIdAlbumMappingForSong().getReleasedYear());
            detailWaitSongById.setTitleAlbum(waitSong.getIdAlbumMappingForSong().getTitleAlbum());
            detailWaitSongById.setArtistName(waitSong.getIdAlbumMappingForSong().getIdArtistMappingForAlbum().getArtistName());
            return GenerateResponse.success("Successfully to get Data", detailWaitSongById);
        }else {
            throw new NotFoundException("Not Found Song with Id: "+ idSong);
        }
    }

    @Override
    public ResponseEntity<Response> deleteWaitSongByIdSong(String idSong, DeleteDto deleteDto) throws JsonProcessingException {
        Optional<WaitSong> optionalSong = waitSongRepository.findWaitSongByIdSong(idSong);
        if (optionalSong.isPresent()){
            WaitSong songUpdate = optionalSong.get();
            if (!songUpdate.isDeleted()){
                songUpdate.setDeleted(true);
                songUpdate.setDeletedBy(deleteDto.getDeletedBy());
                songUpdate.setDeletedAt(LocalDate.now());
                songUpdate.setReasonDeleted(deleteDto.getReasonDeleted());
                waitSongRepository.save(songUpdate);
                return GenerateResponse.success("Successfully to deleted","Song with id: "+idSong);
            }else{
                songUpdate.setDeleted(false);
                waitSongRepository.save(songUpdate);
                return GenerateResponse.success("Successfully to recovery","Song with id: "+idSong);
            }
        }else {
            throw new NotFoundException(idSong);
        }
    }

    @Override
    public ResponseEntity<Response> editStatusApprovedSongById(String idSong, WaitSongStatusApproveDto waitSongStatusApproveDto) throws JsonProcessingException {
        Optional<WaitSong> check = waitSongRepository.findWaitSongByIdSong(idSong);
        if(check.isPresent()){
            WaitSong waitSong = check.get();
            if(Objects.equals(waitSongStatusApproveDto.getStatusApprove().getIsApproved(), "Approved")){
                waitSong.setApproved(true);
                waitSong.setApprovedBy(waitSongStatusApproveDto.getStatusApprove().getApprovedBy());
                waitSong.setApprovedAt(LocalDate.now());
                waitSong.setReasonApproved(waitSongStatusApproveDto.getStatusApprove().getReasonApproved());
                waitSongRepository.save(waitSong);

                Song song = new Song();
                Optional<Album> albumCheck = albumRepository.findAlbumByIdAlbum(waitSong.getIdAlbumMappingForSong().getIdAlbum());

                song.setIdSong(waitSong.getIdSong());
                song.setLanguage(waitSong.getLanguage());
                song.setExplicit(waitSong.getExplicit());
                song.setCreatedBy(waitSong.getCreatedBy());
                song.setTitleSong(waitSong.getTitleSong());
                song.setSong(waitSong.getSong());
                song.setSongType(waitSong.getSongType());
                song.setTotalStreaming(12L);
                song.setTotalDownload(12L);
                song.setDeleted(false);
                song.setCreatedBy(waitSong.getCreatedBy());
                song.setCreatedAt(waitSong.getApprovedAt());
                song.setUpdatedAt(song.getCreatedAt());
                if(albumCheck.isPresent()){
                    song.setIdAlbumMappingForSong(albumCheck.get());
                }else {
                    throw new NotFoundException("Album with ID Album "+ waitSong.getIdAlbumMappingForSong().getIdAlbum()+ " Not Found");
                }


                //Duration Harus di set paling akhir !!!
                song.setDurationSong(waitSong.getDurationSong());

                songRepository.save(song);

                SongWaitEditStatusApproveResponse songWaitEditStatusApproveResponse = getSongWaitEditStatusApproveResponse(waitSong);
                songWaitEditStatusApproveResponse.setArtistName(waitSong.getIdAlbumMappingForSong().getIdArtistMappingForAlbum().getArtistName());
                songWaitEditStatusApproveResponse.setTitleAlbum(waitSong.getIdAlbumMappingForSong().getTitleAlbum());
                songWaitEditStatusApproveResponse.setReleasedYear(waitSong.getIdAlbumMappingForSong().getReleasedYear());

                return GenerateResponse.created("Success to Approve", songWaitEditStatusApproveResponse);


            }else if (Objects.equals(waitSongStatusApproveDto.getStatusReject().getIsRejected(), "Rejected")){
                waitSong.setRejected(true);
                waitSong.setRejectedBy(waitSongStatusApproveDto.getStatusReject().getRejectedBy());
                waitSong.setRejectedAt(LocalDate.now());
                waitSong.setReasonRejected(waitSongStatusApproveDto.getStatusReject().getReasonRejected());
                waitSongRepository.save(waitSong);

                SongWaitEditStatusApproveResponse songWaitEditStatusApproveResponse = getSongWaitEditStatusApproveResponse(waitSong);
                songWaitEditStatusApproveResponse.setArtistName(waitSong.getIdAlbumMappingForSong().getIdArtistMappingForAlbum().getArtistName());
                songWaitEditStatusApproveResponse.setTitleAlbum(waitSong.getIdAlbumMappingForSong().getTitleAlbum());
                songWaitEditStatusApproveResponse.setReleasedYear(waitSong.getIdAlbumMappingForSong().getReleasedYear());

                return GenerateResponse.created("Success to Reject", songWaitEditStatusApproveResponse);

            }
        }
        throw new NotFoundException(idSong);
    }

    private static SongWaitEditStatusApproveResponse getSongWaitEditStatusApproveResponse(WaitSong waitSong) {
        SongWaitEditStatusApproveResponse songWaitEditStatusApproveResponse = new SongWaitEditStatusApproveResponse();
        songWaitEditStatusApproveResponse.setSong(waitSong.getSong());
        songWaitEditStatusApproveResponse.setIdSong(waitSong.getIdSong());
        songWaitEditStatusApproveResponse.setDurationSong(waitSong.getDurationSong());
        songWaitEditStatusApproveResponse.setSongType(waitSong.getSongType());
        songWaitEditStatusApproveResponse.setTitleSong(waitSong.getTitleSong());
        songWaitEditStatusApproveResponse.setApproved(waitSong.isApproved());
        songWaitEditStatusApproveResponse.setDeleted(waitSong.isDeleted());
        songWaitEditStatusApproveResponse.setMadeBy(waitSong.getMadeBy());
        songWaitEditStatusApproveResponse.setExplicit(waitSong.getExplicit());
        songWaitEditStatusApproveResponse.setLanguage(waitSong.getLanguage());
        return songWaitEditStatusApproveResponse;
    }

    @Override
    public ResponseEntity<Response> editSongById(String idSong, WaitSongEditDto waitSongEditDto) throws IOException {
        Optional<WaitSong> check = waitSongRepository.findWaitSongByIdSong(idSong);
        if(check.isPresent()){
            WaitSong waitSong = check.get();

            Optional<WaitAlbum> checkWaitAlbum = waitAlbumRepository.findWaitAlbumByIdAlbum(waitSongEditDto.getIdAlbum());
            if(checkWaitAlbum.isPresent()){
                WaitAlbum waitAlbum = checkWaitAlbum.get();
                waitSong.setIdAlbumMappingForSong(waitAlbum);
            }else {
                throw new NotFoundException("Album with ID Album "+ waitSongEditDto.getIdAlbum()+ " Not Found");
            }
            waitSong.setLanguage(waitSongEditDto.getLanguage());

            waitSong.setExplicit(Boolean.valueOf(waitSongEditDto.getExplicit()));
            waitSong.setCreatedBy(waitSongEditDto.getMadeBy());
            if (waitSongEditDto.getSong()!=null){
                InitWaitDurationSongHandler initWaitDurationSongHandler = new InitWaitDurationSongHandler();

                waitSong.setTitleSong(waitSongEditDto.getSong().getOriginalFilename());
                waitSong.setSong(waitSongEditDto.getSong().getBytes());
                waitSong.setSongType(waitSongEditDto.getSong().getContentType());
                //Duration Harus di set paling akhir !!!
                waitSong.setDurationSong(Time.valueOf(initWaitDurationSongHandler.uploadAudio(waitSongEditDto.getSong())));

            }
            waitSong.setUpdatedBy(waitSong.getUpdatedBy());
            waitSong.setUpdatedAt(LocalDate.now());

            waitSongRepository.save(waitSong);

            SongWaitEditResponse songWaitEditResponse = getSongWaitEditResponse(waitSong);
            songWaitEditResponse.setArtistName(waitSong.getIdAlbumMappingForSong().getIdArtistMappingForAlbum().getArtistName());
            songWaitEditResponse.setTitleAlbum(waitSong.getIdAlbumMappingForSong().getTitleAlbum());
            songWaitEditResponse.setReleasedYear(waitSong.getIdAlbumMappingForSong().getReleasedYear());

            return GenerateResponse.created("Success to edit data", songWaitEditResponse);


        }
        throw new NotFoundException(idSong);
    }

    @Override
    public byte[] viewImageById(String idSong) {
        Optional<WaitSong> exist = waitSongRepository.findWaitSongByIdSong(idSong);
        if (exist.isPresent()) {
            return exist.get().getIdAlbumMappingForSong().getCoverAlbum();

        }
        throw new NotFoundException(idSong);
    }


    private static SongWaitEditResponse getSongWaitEditResponse(WaitSong waitSong) {
        SongWaitEditResponse songWaitEditResponse = new SongWaitEditResponse();
        songWaitEditResponse.setSong(waitSong.getSong());
        songWaitEditResponse.setIdSong(waitSong.getIdSong());
        songWaitEditResponse.setDurationSong(waitSong.getDurationSong());
        songWaitEditResponse.setSongType(waitSong.getSongType());
        songWaitEditResponse.setTitleSong(waitSong.getTitleSong());
        songWaitEditResponse.setApproved(waitSong.isApproved());
        songWaitEditResponse.setDeleted(waitSong.isDeleted());
        songWaitEditResponse.setMadeBy(waitSong.getMadeBy());
        songWaitEditResponse.setExplicit(waitSong.getExplicit());
        songWaitEditResponse.setLanguage(waitSong.getLanguage());
        return songWaitEditResponse;
    }


}



