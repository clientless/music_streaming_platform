//package com.clientless.songmanagement.service;
//
//import be.tarsos.dsp.io.TarsosDSPAudioInputStream;
//import be.tarsos.dsp.io.jvm.JVMAudioInputStreamFactory;
//import be.tarsos.dsp.io.jvm.JVMAudioInputStream;
//import be.tarsos.dsp.pitch.PitchDetectionHandler;
//import be.tarsos.dsp.pitch.PitchDetectionResult;
//import be.tarsos.dsp.pitch.PitchProcessor;
//import be.tarsos.dsp.pitch.mpm.MPM;
//
//import javax.sound.sampled.AudioFileFormat;
//import javax.sound.sampled.AudioInputStream;
//import javax.sound.sampled.AudioSystem;
//import javax.sound.sampled.UnsupportedAudioFileException;
//import java.io.File;
//import java.io.IOException;
//
//public class SongSimilarityChecker {
//
//    public static void main(String[] args) {
//        // Ganti dengan path file lagu Anda
//        String filePath1 = "path_to_song1.mp3";
//        String filePath2 = "path_to_song2.mp3";
//
//        try {
//            double similarity = calculateSimilarity(filePath1, filePath2);
//
//            System.out.println("Similarity between the two songs: " + similarity + "%");
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private static double calculateSimilarity(String filePath1, String filePath2) throws IOException {
//        float[] audioFeatures1 = extractAudioFeatures(filePath1);
//        float[] audioFeatures2 = extractAudioFeatures(filePath2);
//
//        // Implementasikan perhitungan kemiripan antara dua set fitur audio
//        // Contoh sederhana: Membandingkan nilai-nilai fitur audio
//        double matchingFeatures = 0;
//        int totalFeatures = Math.min(audioFeatures1.length, audioFeatures2.length);
//
//        for (int i = 0; i < totalFeatures; i++) {
//            if (audioFeatures1[i] == audioFeatures2[i]) {
//                matchingFeatures++;
//            }
//        }
//
//        return (matchingFeatures / totalFeatures) * 100.0;
//    }
//
//    private static float[] extractAudioFeatures(String filePath) throws IOException, UnsupportedAudioFileException {
//        File audioFile = new File(filePath);
//        AudioInputStream audioStream = AudioSystem.getAudioInputStream(audioFile);
//
//        TarsosDSPAudioInputStream tarsosDSPAudioInputStream = new JVMAudioInputStreamFactory()
//                .createAudioInputStream(audioStream);
//
//        PitchDetectionAlgorithm pitchEstimationAlgorithm = PitchDetectionAlgorithm.MPM;
//        PitchDetectionAlgorithm pitchDetectionAlgorithm = new MPM(tarsosDSPAudioInputStream.getFormat().getSampleRate(),
//                tarsosDSPAudioInputStream.getFormat().getBufferSize());
//
//        PitchProcessor pitchProcessor = new PitchProcessor(pitchDetectionAlgorithm,
//                tarsosDSPAudioInputStream.getFormat().getSampleRate(),
//                tarsosDSPAudioInputStream.getFormat().getBufferSize(),
//                pitchDetectionResult -> {
//                    // Callback untuk hasil deteksi pitch
//                    // Di sini Anda dapat melakukan lebih banyak analisis atau menyimpan nilai-nilai fitur audio
//                });
//
//        // Proses audio stream
//        pitchProcessor.process(tarsosDSPAudioInputStream);
//
//        // Contoh sederhana: Menggunakan array frekuensi pitch sebagai fitur audio
//        float[] pitchValues = pitchProcessor.getPitchGraph().getAverages().clone();
//
//        tarsosDSPAudioInputStream.close();
//
//        return pitchValues;
//    }
//}
