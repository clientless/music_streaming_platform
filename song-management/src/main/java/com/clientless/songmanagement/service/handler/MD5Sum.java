package com.clientless.songmanagement.service.handler;

import java.io.ByteArrayInputStream;
import java.security.DigestInputStream;
import java.security.MessageDigest;

public class MD5Sum {

    public static String generateMD5Checksum(byte[] filePath) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            try (DigestInputStream dis = new DigestInputStream(new ByteArrayInputStream(filePath), md)) {
                while (dis.read() != -1) ;
            }

            byte[] mdBytes = md.digest();

            // Konversi ke format hexadecimal
            StringBuilder sb = new StringBuilder();
            for (byte mdByte : mdBytes) {
                sb.append(Integer.toString((mdByte & 0xff) + 0x100, 16).substring(1));
            }

            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
