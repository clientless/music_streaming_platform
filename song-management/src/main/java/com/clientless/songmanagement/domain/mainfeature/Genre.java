package com.clientless.songmanagement.domain.mainfeature;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "genre_data")

public class Genre {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String genreCode;

    private String genreType;

    @Column(unique = true)
    private String oldPathGenreCode;

    @Column(unique = true)
    private String newPathGenreCode;


    @OneToMany(mappedBy = "genreCodeMappingForAlbum")
    private List<Album> album;
    @OneToMany(mappedBy = "genreCodeMappingForArtist")
    private List<Artist> artists;

    //enhance
    private boolean isDeleted;

    private String createdBy;
    private LocalDate createdAt;
    private String updatedBy;
    private LocalDate updatedAt;
    private String deletedBy;
    private LocalDate deletedAt;
    private String reasonDeleted;

}
