package com.clientless.songmanagement.domain.waitingapproval;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Time;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "wait_song_data")
public class WaitSong {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String idSong;
    private String titleSong;
    //    private String titleAlbum;
    private String language;
    private Boolean explicit;
    //    private String genreType;
    private Time durationSong;
    private String madeBy;
    //    @Lob //Save datatype to type Long
//    @Column(name = "cover_song", columnDefinition = "LONGBLOB")
//    private byte[] coverSong;
//    private String coverType;
    @Lob //Save datatype to type Long
    @Column(columnDefinition = "LONGBLOB")
    private byte[] song;
    private String songType;
    //    private String artistName;
    @ManyToOne
    @JoinColumn(name = "id_album")
    private WaitAlbum idAlbumMappingForSong;

    //enhance
    private boolean isDeleted;
    private boolean isApproved;
    private boolean isRejected;



    private String createdBy;
    private LocalDate createdAt;
    private String updatedBy;
    private LocalDate updatedAt;
    private String deletedBy;
    private LocalDate deletedAt;
    private String reasonDeleted;
    private String approvedBy;
    private LocalDate approvedAt;
    private String reasonApproved;
    private String rejectedBy;
    private LocalDate rejectedAt;
    private String reasonRejected;
    private String remarks;


}
