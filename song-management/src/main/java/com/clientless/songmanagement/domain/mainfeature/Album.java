package com.clientless.songmanagement.domain.mainfeature;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "album_data")
public class Album {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String idAlbum;
    private String titleAlbum;
    private LocalDate releasedYear;
//    private String genreCode;
    private String coverAlbumType;
    @Lob //Save datatype to type Long
    @Column(name = "cover_album", columnDefinition = "LONGBLOB")
    private byte[] coverAlbum;

    @ManyToOne
    @JoinColumn(name = "genre_code")
    private Genre genreCodeMappingForAlbum;

    @ManyToOne
    @JoinColumn(name = "id_artist")
    private Artist idArtistMappingForAlbum;

    @OneToMany(mappedBy = "idAlbumMappingForSong")
    private List<Song> songs;
    private boolean isDeleted;


    private String createdBy;
    private LocalDate createdAt;
    private String updatedBy;
    private LocalDate updatedAt;
    private String deletedBy;
    private LocalDate deletedAt;
    private String reasonDeleted;





}
