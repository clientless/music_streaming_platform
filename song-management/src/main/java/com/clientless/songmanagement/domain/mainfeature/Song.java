package com.clientless.songmanagement.domain.mainfeature;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Time;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "song_data")
public class Song {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String idSong;
    private String titleSong;
    private String madeBy;

    //    private String titleAlbum;
    private String language;
    private Boolean explicit;
//    private String genreType;
    private Time durationSong;
//    @Lob //Save datatype to type Long
//    @Column(name = "cover_song", columnDefinition = "LONGBLOB")
//    private byte[] coverSong;
//    private String coverType;
    @Lob //Save datatype to type Long
    @Column(columnDefinition = "LONGBLOB")
    private byte[] song;
    private String songType;
//    private String artistName;
    private Long totalStreaming;
    private Long totalDownload;
    @ManyToOne
    @JoinColumn(name = "id_album")
    private Album idAlbumMappingForSong;

    //enhance
    private boolean isDeleted;

    private String createdBy;
    private LocalDate createdAt;
    private String updatedBy;
    private LocalDate updatedAt;
    private String deletedBy;
    private LocalDate deletedAt;
    private String reasonDeleted;


}
