package com.clientless.songmanagement.config;

public class AuthenticationConfigConstants {
    public static final String SECRET = "Msp_Climis";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
}
