package com.clientless.songmanagement.dto.waitingapprovalinit.song;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class InitWaitSongStatusSubmitDto {
    private String isSubmitted;
    private String submittedBy;
}
