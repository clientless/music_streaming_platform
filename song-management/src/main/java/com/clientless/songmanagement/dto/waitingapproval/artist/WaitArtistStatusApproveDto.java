package com.clientless.songmanagement.dto.waitingapproval.artist;

import com.clientless.songmanagement.dto.waitingapproval.song.statusApprove.Approved;
import com.clientless.songmanagement.dto.waitingapproval.song.statusApprove.Rejected;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class WaitArtistStatusApproveDto {
    private Rejected statusReject;
    private Approved statusApprove;

}
