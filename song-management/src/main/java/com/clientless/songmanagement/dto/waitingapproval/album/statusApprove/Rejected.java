package com.clientless.songmanagement.dto.waitingapproval.album.statusApprove;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Rejected {
    private String isRejected;
    private String rejectedBy;
    private String reasonRejected;

}
