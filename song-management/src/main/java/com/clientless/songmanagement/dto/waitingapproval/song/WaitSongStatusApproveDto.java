package com.clientless.songmanagement.dto.waitingapproval.song;

import com.clientless.songmanagement.dto.waitingapproval.song.statusApprove.Approved;
import com.clientless.songmanagement.dto.waitingapproval.song.statusApprove.Rejected;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WaitSongStatusApproveDto {
    private Rejected statusReject;
    private Approved statusApprove;
}
