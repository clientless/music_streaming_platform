package com.clientless.songmanagement.dto.mainfeature;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SongDto {
    private String titleSong;
    private String idAlbum;
    private String language;
    private String explicit;
    private String madeBy;
    private String durationSong;
    private MultipartFile song;
    private Long totalStreaming;
    private Long totalDownload;
    private String createdBy;
    private String updatedBy;
    private String deletedBy;
    private String reasonDeleted;


}
