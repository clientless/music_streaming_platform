package com.clientless.songmanagement.dto.waitingapproval.song.statusApprove;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Approved {
    private String isApproved;
    private String approvedBy;
    private String reasonApproved;
}
