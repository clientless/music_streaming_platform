package com.clientless.songmanagement.dto.waitingapprovalinit.album;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class InitWaitAlbumStatusSubmitDto {
    private String isSubmitted;
    private String submittedBy;

}
