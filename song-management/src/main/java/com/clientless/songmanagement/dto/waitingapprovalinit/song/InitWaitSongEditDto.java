package com.clientless.songmanagement.dto.waitingapprovalinit.song;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InitWaitSongEditDto {
    private String titleSong;
    private String idAlbum;
    private String language;
    private String explicit;
    private String madeBy;
    private MultipartFile song;
    private String updatedBy;

}
