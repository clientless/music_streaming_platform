package com.clientless.songmanagement.controller;

import com.clientless.songmanagement.service.handler.MD5SumComparator;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/album")
public class MD5SumChecker {
    private final MD5SumComparator md5SumComparator;

    public MD5SumChecker(MD5SumComparator md5SumComparator) {
        this.md5SumComparator = md5SumComparator;
    }

    @GetMapping(value = "/md5Sum",produces = MediaType.APPLICATION_JSON_VALUE)
    public String listAlbum() {
        return md5SumComparator.result();
    }
}
