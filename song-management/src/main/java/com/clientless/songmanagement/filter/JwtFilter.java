package com.clientless.songmanagement.filter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.clientless.songmanagement.config.AuthenticationConfigConstants;
import com.clientless.songmanagement.exception.ForbiddenCustomExceptionJWT;
import com.clientless.songmanagement.exception.UnauthorizedCustomExceptionJWT;
import io.jsonwebtoken.JwtException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

public class JwtFilter extends BasicAuthenticationFilter {


    public JwtFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String header = request.getHeader(AuthenticationConfigConstants.HEADER_STRING);

        if ((header == null || !header.startsWith(AuthenticationConfigConstants.TOKEN_PREFIX)) &&
                (!request.getRequestURI().startsWith("/v1/song/play_song/") &&
                        !request.getRequestURI().startsWith("/v1/wait_song/play_song/") &&
                        !request.getRequestURI().startsWith("/v1/init_wait_song/play_song/"))) {
            UnauthorizedCustomExceptionJWT unauthorizedCustomExceptionJWT = new UnauthorizedCustomExceptionJWT();
            unauthorizedCustomExceptionJWT.generateError(response);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(request, response);

        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(request, response);
    }


    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String token = request.getHeader(AuthenticationConfigConstants.HEADER_STRING);
        if (token != null) {
            // parse the token.
            try {
                DecodedJWT verify = JWT.require(Algorithm.HMAC512(AuthenticationConfigConstants.SECRET.getBytes())).build().verify(token.replace(AuthenticationConfigConstants.TOKEN_PREFIX, ""));
                String username = verify.getSubject();
                String role = verify.getClaim("role").asString();

                if (username != null) {
                    return new UsernamePasswordAuthenticationToken(username, null, getAuthorities(role));
                }
                return null;
            } catch (TokenExpiredException tee) {
                String errorMsq = tee.getMessage();
                ForbiddenCustomExceptionJWT forbiddenCustomExceptionJWT = new ForbiddenCustomExceptionJWT();
                forbiddenCustomExceptionJWT.generateError(response, errorMsq);

            } catch (JwtException e) {

                throw new BadCredentialsException("Unauthorized: " + e.getMessage(), e);
            }
        }
        return null;

    }

    private Collection<? extends GrantedAuthority> getAuthorities(String role) {
        return List.of(new SimpleGrantedAuthority(role));
    }

}
