package com.clientless.songmanagement.exception;

import com.clientless.songmanagement.model.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;

@ControllerAdvice
public class ExceptionHelper {
    private static final Logger logger = LoggerFactory.getLogger(ExceptionHelper.class);

    @ExceptionHandler(value = { HttpClientErrorException.Unauthorized.class })
    public ResponseEntity<Response> handleUnauthorizedException(HttpClientErrorException.Unauthorized ex) {
        logger.error("Exception: ",ex.getMessage());
        return new ResponseEntity<>(new Response(ex.getMessage(), null, HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = { HttpClientErrorException.Forbidden.class })
    public ResponseEntity<Response> handleForbiddenException(HttpClientErrorException.Unauthorized ex) {
        logger.error("Exception: ",ex.getMessage());
        return new ResponseEntity<>(new Response(ex.getMessage(), null, HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = { NotFoundException.class })
    public ResponseEntity<Response> handleNotFoundException(NotFoundException ex) {
        logger.error("Not Found Exception: ",ex.getMessage());
        return new ResponseEntity<>(new Response(ex.getMessage(), null,HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
    }
    @ExceptionHandler(value = { NotFoundExceptionCausesDelete.class })
    public ResponseEntity<Response> handleNotFoundExceptionCausesDelete(NotFoundExceptionCausesDelete ex) {
        logger.error("Not Found Exception: ",ex.getMessage());
        return new ResponseEntity<>(new Response(ex.getMessage(), null,HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = { MissingServletRequestParameterException.class })
    public ResponseEntity<Response> handleBadRequestException(MissingServletRequestParameterException ex) {
        logger.error("Bad Request Exception: ",ex.getMessage());
        return new ResponseEntity<>(new Response(ex.getMessage(), null, HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
    }

}
