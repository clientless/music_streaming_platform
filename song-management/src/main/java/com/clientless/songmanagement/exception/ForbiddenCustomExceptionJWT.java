package com.clientless.songmanagement.exception;

import com.clientless.songmanagement.model.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ForbiddenCustomExceptionJWT {
    private static final Logger logger = LoggerFactory.getLogger(ForbiddenCustomExceptionJWT.class);

    public void generateError(HttpServletResponse response, String errMsg) throws IOException {
        Response objResponse = new Response(errMsg, null, HttpStatus.FORBIDDEN.value());
        String returnToString = objResponse.toString();
        response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        response.setContentType("application/json");
        response.getWriter().write(returnToString);
        response.getWriter().flush();
        response.getWriter().close();
        logger.error(returnToString);
    }


}
