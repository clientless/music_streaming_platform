package org.msp.connectordatasync;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
//@EnableScheduling
//@ComponentScan(basePackages = "org.msp.connectordatasync.service")
public class ConnectorDataSyncApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConnectorDataSyncApplication.class, args);
    }

}
