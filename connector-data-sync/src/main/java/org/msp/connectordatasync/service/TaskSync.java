package org.msp.connectordatasync.service;

import org.msp.connectordatasync.domain.AdminDomain;
import org.msp.connectordatasync.domain.PicDomain;
import org.msp.connectordatasync.domain.UserDomain;
import org.msp.connectordatasync.domain.UsersDomain;
import org.msp.connectordatasync.model.dto.UsersDto;
import org.msp.connectordatasync.repository.AdminRepository;
import org.msp.connectordatasync.repository.PicRepository;
import org.msp.connectordatasync.repository.UserRepository;
import org.msp.connectordatasync.repository.UsersRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class TaskSync {
    private final AdminRepository adminRepository;
    private final UserRepository userRepository;
    private final PicRepository picRepository;
    private final UsersRepository usersRepository;

    private final Logger logger = LoggerFactory.getLogger(TaskSync.class);

    public TaskSync(AdminRepository adminRepository, UserRepository userRepository, PicRepository picRepository, UsersRepository usersRepository) {
        this.adminRepository = adminRepository;
        this.userRepository = userRepository;
        this.picRepository = picRepository;
        this.usersRepository = usersRepository;
    }

    @Async
    @Scheduled(cron = "56 8 * * * *") // Tiap 3 hari di jam 12 malam
//    @Transactional
    public void insertDataAdminToUsersTable() {
        // Ambil data dari tiga tabel
        List<AdminDomain> adminDomainList = adminRepository.findAll();
        int counterAdmSuccess = 0;
        int counterAdmFailed = 0;
        logger.info("Start Counter Admin Total: " + adminDomainList.size());
        for (int i = 0; i < adminDomainList.size(); i++) {
            try {
                UsersDto usersDto = new UsersDto();
                usersDto.setEmail(adminDomainList.get(i).getEmail());
                usersDto.setRole(adminDomainList.get(i).getRole());
                usersDto.setUsername(adminDomainList.get(i).getUsername());
                usersDto.setPassword(adminDomainList.get(i).getPassword());
                usersDto.setPkIdSource(adminDomainList.get(i).getId());
                usersDto.setStatus(adminDomainList.get(i).isStatus());

                Optional<UsersDomain> usersDomainOptional = usersRepository.findByPkIdSourceAndRole(usersDto.getPkIdSource(), usersDto.getRole());
                if (usersDomainOptional.isEmpty()) {
                    UsersDomain usersDomain = new UsersDomain();
                    usersDomain.setUsername(usersDto.getUsername());
                    usersDomain.setRole(usersDto.getRole());
                    usersDomain.setPassword(usersDto.getPassword());
                    usersDomain.setEmail(usersDto.getEmail());
                    usersDomain.setPkIdSource(usersDto.getPkIdSource());
                    usersDomain.setStatus(usersDto.isStatus());
                    usersRepository.save(usersDomain);

                } else {
                    UsersDomain usersDomain = usersDomainOptional.get();
                    usersDomain.setUsername(usersDto.getUsername());
                    usersDomain.setRole(usersDto.getRole());
                    usersDomain.setPassword(usersDto.getPassword());
                    usersDomain.setEmail(usersDto.getEmail());
                    usersDomain.setPkIdSource(usersDto.getPkIdSource());
                    usersDomain.setStatus(usersDto.isStatus());
                    usersRepository.save(usersDomain);
                }
                counterAdmSuccess++;
            } catch (Exception e) {
                counterAdmFailed++;
            }

        }
        logger.info("Total Counter Admin: " + adminDomainList.size());
        logger.info("Success Counter Admin: " + counterAdmSuccess);
        logger.info("failed Counter Admin: " + counterAdmFailed);

    }

    @Async
    @Scheduled(cron = "56 8 * * * *") // Tiap 3 hari di jam 12 malam
//    @Transactional
    public void insertDataPicToUsersTable() {
        // Ambil data dari tiga tabel
        List<PicDomain> picDomainList = picRepository.findAll();
        int counterPicSuccess = 0;
        int counterPicFailed = 0;
        logger.info("Start Counter Pic Total: " + picDomainList.size());

        for (int i = 0; i < picDomainList.size(); i++) {
            try {
                UsersDto usersDto = new UsersDto();
                usersDto.setEmail(picDomainList.get(i).getEmail());
                usersDto.setRole(picDomainList.get(i).getRole());
                usersDto.setUsername(picDomainList.get(i).getUsername());
                usersDto.setPassword(picDomainList.get(i).getPassword());
                usersDto.setPkIdSource(picDomainList.get(i).getId());
                usersDto.setStatus(picDomainList.get(i).isStatus());

                Optional<UsersDomain> usersDomainOptional = usersRepository.findByPkIdSourceAndRole(usersDto.getPkIdSource(), usersDto.getRole());
                if (usersDomainOptional.isEmpty()) {
                    UsersDomain usersDomain = new UsersDomain();
                    usersDomain.setUsername(usersDto.getUsername());
                    usersDomain.setRole(usersDto.getRole());
                    usersDomain.setPassword(usersDto.getPassword());
                    usersDomain.setEmail(usersDto.getEmail());
                    usersDomain.setPkIdSource(usersDto.getPkIdSource());
                    usersDomain.setStatus(usersDto.isStatus());
                    usersRepository.save(usersDomain);

                } else {
                    UsersDomain usersDomain = usersDomainOptional.get();
                    usersDomain.setUsername(usersDto.getUsername());
                    usersDomain.setRole(usersDto.getRole());
                    usersDomain.setPassword(usersDto.getPassword());
                    usersDomain.setEmail(usersDto.getEmail());
                    usersDomain.setPkIdSource(usersDto.getPkIdSource());
                    usersDomain.setStatus(usersDto.isStatus());
                    usersRepository.save(usersDomain);
                }
                counterPicSuccess++;

            } catch (Exception e) {
                counterPicFailed++;

            }
        }
        logger.info("Total Counter Pic: " + picDomainList.size());
        logger.info("Success Counter Pic: " + counterPicSuccess);
        logger.info("failed Counter Pic: " + counterPicFailed);

    }

    @Async
    @Scheduled(cron = "56 8 * * * *") // Tiap 3 hari di jam 12 malam
//    @Transactional
    public void insertDataUserToUsersTable() {
        // Ambil data dari tiga tabel
        List<UserDomain> userDomainList = userRepository.findAll();
        int counterUserSuccess = 0;
        int counterUserFailed = 0;
        logger.info("Start Counter User Total: " + userDomainList.size());

        for (int i = 0; i < userDomainList.size(); i++) {
            try {
                UsersDto usersDto = new UsersDto();
                usersDto.setEmail(userDomainList.get(i).getEmail());
                usersDto.setRole(userDomainList.get(i).getRole());
                usersDto.setUsername(userDomainList.get(i).getUsername());
                usersDto.setPassword(userDomainList.get(i).getPassword());
                usersDto.setPkIdSource(userDomainList.get(i).getId());
                usersDto.setStatus(userDomainList.get(i).isStatus());

                Optional<UsersDomain> usersDomainOptional = usersRepository.findByPkIdSourceAndRole(usersDto.getPkIdSource(), usersDto.getRole());
                if (usersDomainOptional.isEmpty()) {
                    UsersDomain usersDomain = new UsersDomain();
                    usersDomain.setUsername(usersDto.getUsername());
                    usersDomain.setRole(usersDto.getRole());
                    usersDomain.setPassword(usersDto.getPassword());
                    usersDomain.setEmail(usersDto.getEmail());
                    usersDomain.setPkIdSource(usersDto.getPkIdSource());
                    usersDomain.setStatus(usersDto.isStatus());
                    usersRepository.save(usersDomain);

                } else {
                    UsersDomain usersDomain = usersDomainOptional.get();
                    usersDomain.setUsername(usersDto.getUsername());
                    usersDomain.setRole(usersDto.getRole());
                    usersDomain.setPassword(usersDto.getPassword());
                    usersDomain.setEmail(usersDto.getEmail());
                    usersDomain.setPkIdSource(usersDto.getPkIdSource());
                    usersDomain.setStatus(usersDto.isStatus());
                    usersRepository.save(usersDomain);
                }
                counterUserSuccess++;
            }catch (Exception e){
                counterUserFailed++;

            }
        }
        logger.info("Total Counter User: " + userDomainList.size());
        logger.info("Success Counter User: " + counterUserSuccess);
        logger.info("failed Counter User: " + counterUserFailed);

    }

}

