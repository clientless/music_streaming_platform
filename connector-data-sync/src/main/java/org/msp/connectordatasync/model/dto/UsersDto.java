package org.msp.connectordatasync.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UsersDto {
//    private Long id;
    private String email;
    private String username;
    private String password;
    private String role;
    private Long pkIdSource;
    private boolean status;
}
