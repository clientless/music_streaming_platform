package org.msp.connectordatasync.model.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDao {
    private Long id;

    private String email;

    private String username;

    private String fullname;

    private String password;

    private boolean status;
    private String role ;
}
