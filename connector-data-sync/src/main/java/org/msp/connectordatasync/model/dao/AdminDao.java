package org.msp.connectordatasync.model.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdminDao {
    private Long id;
    private String email;
    private String username;
    private String fullname;
    private String emailCompany;
    private String password;
    private boolean status;
    private String role;
}
