package org.msp.connectordatasync.repository;

import org.msp.connectordatasync.domain.PicDomain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PicRepository extends JpaRepository<PicDomain, Long> {
    PicDomain findByUsername(String username);
    PicDomain findByUsernameAndStatus(String username, boolean status);


    PicDomain findByEmailAndStatus(String Email, Boolean status);
}
