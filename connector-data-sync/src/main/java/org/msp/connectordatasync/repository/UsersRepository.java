package org.msp.connectordatasync.repository;

import org.msp.connectordatasync.domain.UsersDomain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UsersRepository extends JpaRepository<UsersDomain, Long> {
    UsersDomain findByUsername(String username);

    UsersDomain findByEmail(String email);

    UsersDomain findByUsernameAndStatus(String username, boolean status);

    UsersDomain findByEmailAndStatus(String email, Boolean status);

    Optional<UsersDomain> findByPkIdSourceAndRole(Long pkIdSource, String role);
}
