package org.msp.connectordatasync.repository;

import org.msp.connectordatasync.domain.AdminDomain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminRepository extends JpaRepository<AdminDomain, Long> {
    AdminDomain findByUsername(String username);
    AdminDomain findByUsernameAndStatus(String username,boolean status);

    @Query("SELECT a FROM AdminDomain a WHERE a.username = :username OR a.emailCompany = :emailCompany")
    AdminDomain findByUsernameOrEmailCompany(@Param("username") String username, @Param("emailCompany") String emailCompany);

}
