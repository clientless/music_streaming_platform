package org.msp.connectordatasync.controller;

import org.msp.connectordatasync.service.TaskSync;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sync/data/users")
public class AdminSyncController {
    private final TaskSync taskSync;

    public AdminSyncController(TaskSync taskSync) {
        this.taskSync = taskSync;
    }

    @GetMapping("/task/admin")
    public String hitService(){
        taskSync.insertDataAdminToUsersTable();
        taskSync.insertDataPicToUsersTable();
        taskSync.insertDataUserToUsersTable();
        return "OK";
    }
}
