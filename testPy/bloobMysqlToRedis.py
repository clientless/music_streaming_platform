import mysql.connector
import io
import soundfile as sf
import redis
import librosa
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity

def extract_features_from_redis(redis_conn, key, target_duration):
    audio_bytes = redis_conn.get(key)
    y, sr = librosa.load(io.BytesIO(audio_bytes), duration=target_duration, sr=None)
    mfccs = librosa.feature.mfcc(y=y, sr=sr, n_mfcc=13)
    return mfccs

def calculate_similarity(feature1, feature2):
    flat_feature1 = feature1.flatten().reshape(1, -1)
    flat_feature2 = feature2.flatten().reshape(1, -1)
    similarity = cosine_similarity(flat_feature1, flat_feature2)
    return similarity[0][0]

# 1. Ambil Data BLOB dari MySQL
conn = mysql.connector.connect(
    host='localhost',
    user='root',
    password='Password12345@',
    database='db_csm_song_mg',
    port=3306
)


cursor = conn.cursor()
query = "SELECT audio_blob FROM your_table WHERE your_condition"
cursor.execute(query)
audio_blob1 = cursor.fetchone()[0]
audio_blob2 = cursor.fetchone()[0]  # Ganti dengan query atau metode lain sesuai kebutuhan
cursor.close()
conn.close()

# 2. Konversi BLOB ke File Audio dan Simpan ke Redis
redis_conn = redis.StrictRedis(host='localhost', port=6379, db=0)

# Simpan audio_blob1 ke Redis
audio_data1 = io.BytesIO(audio_blob1)
sf.write('output_audio1.wav', audio_data1.read(), samplerate=44100)
with open('output_audio1.wav', 'rb') as audio_file1:
    audio_bytes1 = audio_file1.read()
redis_conn.set('audio_key1', audio_bytes1)

# Simpan audio_blob2 ke Redis
audio_data2 = io.BytesIO(audio_blob2)
sf.write('output_audio2.wav', audio_data2.read(), samplerate=44100)
with open('output_audio2.wav', 'rb') as audio_file2:
    audio_bytes2 = audio_file2.read()
redis_conn.set('audio_key2', audio_bytes2)

# 3. Proses File Audio Menggunakan Librosa untuk Periksa Kemiripan
path_redis_audio1 = 'audio_key1'
path_redis_audio2 = 'audio_key2'
duration_redis_audio1 = librosa.get_duration(filename=path_redis_audio1)
duration_redis_audio2 = librosa.get_duration(filename=path_redis_audio2)
target_duration_redis = max(duration_redis_audio1, duration_redis_audio2)

# Ekstraksi fitur dari file audio di Redis
feature_redis_audio1 = extract_features_from_redis(redis_conn, 'audio_key1', target_duration_redis)
feature_redis_audio2 = extract_features_from_redis(redis_conn, 'audio_key2', target_duration_redis)

# Menghitung kemiripan antara dua audio dari Redis
similarity_redis = calculate_similarity(feature_redis_audio1, feature_redis_audio2)

print(f"Similarity between the Redis audios: {similarity_redis}")
