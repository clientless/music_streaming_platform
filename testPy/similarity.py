# import essentia.pytools.extractors.music_extractor
# import sklearn.metrics.pairwise
# from essentia.standard import *
# from sklearn.metrics.pairwise import *
#
# # Load lagu audio (pastikan lagu berformat WAV)
# audio_path_1 = '/home/lukitoandriansyah/BackUp/eLA99/upload_music/5mile~s.wav'
# audio_path_2 = '/home/lukitoandriansyah/BackUp/eLA99/upload_music/Yokan.wav'
#
# # Ekstraksi fitur-fitur
# extractor = essentia.pytools.extractors.music_extractor.music_extractor()
#
# # Menggunakan extractor untuk mendapatkan fitur-fitur
# features_1 = extractor(audio_path_1)
# features_2 = extractor(audio_path_2)
#
# # Mengukur kemiripan dengan Cosine Similarity
# cosine_similarity = sklearn.metrics.pairwise.cosine_similarity()
# similarity = cosine_similarity(features_1[0], features_2[0])  # Mengambil fitur pertama (biasanya mel-frequency cepstral coefficients)
#
# # Print hasil kemiripan
# print(f'Cosine Similarity antara lagu 1 dan lagu 2: {similarity}')
