import librosa
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity

def extract_features(file_path, target_duration):
    y, sr = librosa.load(file_path, duration=target_duration, sr=None)
    mfccs = librosa.feature.mfcc(y=y, sr=sr, n_mfcc=13)
    return mfccs

def calculate_similarity(feature1, feature2):
    flat_feature1 = feature1.flatten().reshape(1, -1)
    flat_feature2 = feature2.flatten().reshape(1, -1)
    similarity = cosine_similarity(flat_feature1, flat_feature2)
    return similarity[0][0]

# Path ke dua lagu yang ingin dibandingkan
path_song1 = r'C:\Program Files\private\Backup_From_Ubuntu_pt1\Home\Backup_Lukito\BackUp\quadtrikal\Album Raw_Utopia\Lagu\Mp3\Bintang_Jatuh.mp3'
path_song2 = r'C:\Program Files\private\Backup_From_Ubuntu_pt1\Home\Backup_Lukito\BackUp\quadtrikal\Album Raw_Utopia\Lagu\Mp3\Chubby (Remastered).mp3'

path_song3 = r'../'
# Hitung durasi lagu terpanjang
duration_song1 = librosa.get_duration(path=path_song1)
duration_song2 = librosa.get_duration(path=path_song2)
target_duration = max(duration_song1, duration_song2)

# Ekstraksi fitur dari dua lagu dengan durasi yang sama (dengan padding zeros)
feature_song1 = extract_features(path_song1, target_duration)
feature_song2 = extract_features(path_song2, target_duration)

# Ensure both features have the same number of frames by padding zeros
max_frames = max(feature_song1.shape[1], feature_song2.shape[1])
feature_song1 = np.pad(feature_song1, ((0, 0), (0, max_frames - feature_song1.shape[1])), mode='constant', constant_values=0)
feature_song2 = np.pad(feature_song2, ((0, 0), (0, max_frames - feature_song2.shape[1])), mode='constant', constant_values=0)

# Reshape to 2D array
feature_song1 = feature_song1.reshape(-1, max_frames)
feature_song2 = feature_song2.reshape(-1, max_frames)

# Menghitung kemiripan
similarity = calculate_similarity(feature_song1, feature_song2)

print(f"Similarity between the songs: {similarity}")
