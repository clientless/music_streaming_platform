# python_scripts/audio_features.py

import sys
import librosa
import numpy as np

def extract_features(file_path, target_duration):
    y, sr = librosa.load(file_path, duration=target_duration, sr=None)
    mfccs = librosa.feature.mfcc(y=y, sr=sr, n_mfcc=13)
    return mfccs.tolist()

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python audio_features.py <file_path> <target_duration>")
        sys.exit(1)

    file_path = sys.argv[1]
    target_duration = float(sys.argv[2])

    features = extract_features(file_path, target_duration)
    print(features)
