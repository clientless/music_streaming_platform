# controller/audio_controller.py
from flask import jsonify
from service.audio_service import process_audio_service
from middleware.error_handler import handle_error


def process_audio_endpoint(request):
    try:
        # Retrieve parameters from Spring
        id_song_compare = request.json.get('idSongCompare')
        id_song_comparator = request.json.get('idSongComparator')

        # Call the service layer
        result = process_audio_service(id_song_compare[0], id_song_comparator[0])

        return jsonify(result)
        # return result

    except Exception as e:
        return handle_error(e)
