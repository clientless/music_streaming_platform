# service/audio_service.py
from repository.audio_repository_redis import store_in_redis, delete_from_redis, increment_retry_count
from repository.audio_repository_mysql import retrieve_audio_data_compare, retrieve_audio_data_comparator
from util.calculate_similarity import calculate_similarity

# Maksimal percobaan yang diizinkan sebelum memberikan kesalahan
MAX_RETRY = 3


def process_audio_service(id_song_compare, id_song_comparator):
    # Fetch audio data from MySQL based on parameters
    audio_compare = retrieve_audio_data_compare(id_song_compare)
    audio_comparator = retrieve_audio_data_comparator(id_song_comparator)

    # Process audio data
    processed_compare = process_audio(audio_compare)
    processed_comparator = process_audio(audio_comparator)

    if audio_compare == b'' and audio_comparator == b'':
        return {"ERROR_CODE": "ERR003.CO.COR", "ID_COMPARE": id_song_compare, "ID_COMPARATOR":id_song_comparator}
    elif audio_comparator == b'':
        return {"ERROR_CODE": "ERR003.COR", "ID_COMPARE": id_song_compare, "ID_COMPARATOR":id_song_comparator}
    elif audio_compare == b'':
        return {"ERROR_CODE": "ERR003.CO", "ID_COMPARE": id_song_compare, "ID_COMPARATOR":id_song_comparator}


    # Store processed audio in Redis
    key_compare = store_in_redis(processed_compare)
    key_comparator = store_in_redis(processed_comparator)

    # Initialize retry count
    retry_count = 0

    while retry_count < MAX_RETRY:
        try:
            # Compare audio files
            similarity = calculate_similarity(processed_compare, processed_comparator)

            # result = {"message": "Audio processing successful", "similarity": str(round(similarity * 100, 4)) + "%"}
            result = round(similarity * 100, 4)

            # Delete from Redis after successful comparison
            delete_from_redis(key_compare, key_comparator)
            return result

        except Exception as e:
            # Handle error and retry
            retry_count += 1
            increment_retry_count(key_compare)
            increment_retry_count(key_comparator)

    # If all retries fail, raise an error or handle it accordingly
    # raise Exception("ERR004")
    return "ERR004"


def process_audio(audio_data):
    # Implement your audio processing logic here
    # For example, converting the audio format, extracting features, etc.
    # Return the processed audio data
    return audio_data
