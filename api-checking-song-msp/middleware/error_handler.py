# middleware/error_handler.py
from flask import jsonify


def handle_error(error):
    print(f"Error processing audio: {str(error)}")
    result = {"message": "Error processing audio"}
    return jsonify(result), 500
