from middleware.mysql_handler import connection


def retrieve_audio_data_from_mysql_wait_song(song_id):
    # Fetch audio data from MySQL
    with connection.cursor() as cursor:
        query = "SELECT song FROM wait_song_data WHERE id_song = %s"
        cursor.execute(query, (song_id,))
        audio_data = cursor.fetchone()

    return audio_data


def retrieve_audio_data_from_mysql_song(song_id):
    # Fetch audio data from MySQL
    with connection.cursor() as cursor:
        query = "SELECT song FROM song_data WHERE id_song = %s"
        cursor.execute(query, (song_id,))
        audio_data = cursor.fetchone()

    return audio_data


def retrieve_audio_data_comparator(song_id):
    if retrieve_audio_data_from_mysql_song(song_id) is not None:
        return retrieve_audio_data_from_mysql_song(song_id)[0]
    else:
        return b''


def retrieve_audio_data_compare(song_id):
    if retrieve_audio_data_from_mysql_song(song_id) is not None:
        return retrieve_audio_data_from_mysql_song(song_id)[0]
    elif retrieve_audio_data_from_mysql_wait_song(song_id) is not None:
        return retrieve_audio_data_from_mysql_wait_song(song_id)[0]
    else:
        return b''
