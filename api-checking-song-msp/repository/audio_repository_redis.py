from middleware.redis_handler import redis_conn
from util.key_generator import generate_unique_key


def store_in_redis(processed_audio):
    # Generate a unique key for Redis
    redis_key = generate_unique_key()

    # Store processed audio in Redis
    redis_conn.set(redis_key, processed_audio)

    return redis_key


def delete_from_redis(key1, key2):
    # Delete processed audio in Redis
    redis_conn.delete(key1)
    redis_conn.delete(key2)

    return ""


def get_retry_count(key):
    # Dapatkan jumlah percobaan dari Redis Hash
    retry_count = redis_conn.hget("retry_counts", key)
    return int(retry_count) if retry_count else 0


def increment_retry_count(key):
    # Increment retry count in Redis
    retry_count = redis_conn.incr(f"{key}_retry_count")
    return retry_count


def reset_retry_count(key):
    # Reset jumlah percobaan ke 0 dalam Redis Hash
    redis_conn.hset("retry_counts", key, 0)
