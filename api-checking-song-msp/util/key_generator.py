import time


def generate_unique_key():
    # Implement a logic to generate a unique key
    # You can use a combination of timestamp, random number, or any other method
    # For simplicity, using a timestamp as an example
    timestamp = int(time.time())
    return f"audio_key_{timestamp}"
