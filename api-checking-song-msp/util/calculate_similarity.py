from sklearn.metrics.pairwise import cosine_similarity
import numpy as np


def calculate_similarity(audio1, audio2):
    # Convert bytes to NumPy arrays
    array_feature1 = np.frombuffer(audio1, dtype=np.float32)
    array_feature2 = np.frombuffer(audio2, dtype=np.float32)

    # Handle NaN values by replacing them with 0
    array_feature1 = np.nan_to_num(array_feature1, nan=0.0)
    array_feature2 = np.nan_to_num(array_feature2, nan=0.0)

    # Find the maximum length of the two arrays
    max_length = max(len(array_feature1), len(array_feature2))

    # Pad the shorter array with zeros to make them of equal length
    array_feature1 = np.pad(array_feature1, (0, max_length - len(array_feature1)))
    array_feature2 = np.pad(array_feature2, (0, max_length - len(array_feature2)))

    # Reshape the arrays if needed
    # For example, if your data represents a 2D array, you might need to reshape it.
    # array_feature1 = array_feature1.reshape((num_rows, num_columns))
    # array_feature2 = array_feature2.reshape((num_rows, num_columns))

    # Flatten the arrays
    flat_feature1 = array_feature1.flatten().tolist()
    flat_feature2 = array_feature2.flatten().tolist()

    # Calculate similarity
    similarity = cosine_similarity([flat_feature1], [flat_feature2])

    return similarity[0][0]
