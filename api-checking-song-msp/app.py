# app.py

from flask import Flask, request
from controller.audio_controller import process_audio_endpoint

app = Flask(__name__)

@app.route('/process_audio', methods=['POST'])
def process_audio():
    return process_audio_endpoint(request)


# ...


if __name__ == '__main__':
    app.run(port=5000)
