package com.clientless.usersplatform.controller;

import com.clientless.usersplatform.UserApplication;
import com.clientless.usersplatform.domain.PicDomain;
import com.clientless.usersplatform.dto.pic.PicRegisterDto;
import com.clientless.usersplatform.dto.pic.PicUpdateDto;
import com.clientless.usersplatform.model.Response;
import com.clientless.usersplatform.repository.PicRepository;
import com.clientless.usersplatform.security.BCrypt;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = UserApplication.class)
@AutoConfigureMockMvc
class PicDomainControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private PicRepository picRepository;
    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        picRepository.deleteAll();

        PicDomain picDomain = new PicDomain();
        picDomain.setEmail("pic1@example.com");
        picDomain.setPassword(BCrypt.hashpw("rahasia", BCrypt.gensalt()));
        picDomain.setUsername("pic1");
        picDomain.setFullname("Pic Satu");
        picDomain.setStatus(true);

        picRepository.save(picDomain);
    }

    @AfterEach
    void tearDown() {
        picRepository.deleteAll();
    }

    @Test
    public void testRegisterPicBadRequest() throws Exception {
        // Blank email & password
        PicRegisterDto request = new PicRegisterDto();
        request.setEmail("");
        request.setPassword("");

        mockMvc.perform(
                post("/v1/pic/signup")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
        ).andExpect(
                status().isBadRequest()
        ).andDo(result -> {
            Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                    new TypeReference<>() {});

            assertEquals(400, response.getCode());
            assertNull(response.getData());
            assertNotNull(response.getMessage());
        });

        // Wrong format email
        request = new PicRegisterDto();
        request.setEmail("wrong-format");
        request.setPassword("rahasia");

        mockMvc.perform(
                post("/v1/pic/signup")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
        ).andExpect(
                status().isBadRequest()
        ).andDo(result -> {
            Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                    new TypeReference<>() {});

            assertEquals(400, response.getCode());
            assertNull(response.getData());
            assertNotNull(response.getMessage());
        });
    }

    @Test
    public void testRegisterPicUsedEmail() throws Exception {
        // Used account
        PicRegisterDto request = new PicRegisterDto();
        request.setEmail("pic1@example.com");
        request.setPassword("rahasia");

        mockMvc.perform(
                post("/v1/pic/signup")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
        ).andExpect(
                status().isImUsed()
        ).andDo(result -> {
            Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                    new TypeReference<>() {});

            assertEquals(226, response.getCode());
            assertNull(response.getData());
            assertNotNull(response.getMessage());
        });
    }

    @Test
    public void testRegisterPicSuccess() throws Exception {
        // Used account
        PicRegisterDto request = new PicRegisterDto();
        request.setEmail("pic2@example.com");
        request.setPassword("rahasia");
        request.setFullname("Pic Dua");

        mockMvc.perform(
                post("/v1/pic/signup")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
        ).andExpect(
                status().isCreated()
        ).andDo(result -> {
            Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                    new TypeReference<>() {});

            assertEquals(201, response.getCode());
            assertNotNull(response.getData());
            assertEquals("Registration success", response.getMessage());
        });
    }

    @Test
    public void testGetPicNotFound() throws Exception {
        mockMvc.perform(
                get("/v1/pic/salah")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(
                status().isNotFound()
        ).andDo(result -> {
            Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                    new TypeReference<>() {});

            assertEquals(404, response.getCode());
            assertNull(response.getData());
            assertNotNull(response.getMessage());
        });
    }

    @Test
    public void testGetPicSuccess() throws Exception {
        mockMvc.perform(
                get("/v1/pic/pic1")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(
                status().isOk()
        ).andDo(result -> {
            Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                    new TypeReference<>() {});

            assertEquals(200, response.getCode());
            assertNotNull(response.getData());
            assertEquals("Data found", response.getMessage());
        });
    }

    @Test
    public void testUpdatePicBadRequest() throws Exception {
        PicUpdateDto request = new PicUpdateDto();
        request.setEmail("wrong-format");

        mockMvc.perform(
                put("/v1/pic/pic1")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
        ).andExpect(
                status().isBadRequest()
        ).andDo(result -> {
            Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                    new TypeReference<>() {});

            assertEquals(400, response.getCode());
            assertNull(response.getData());
            assertNotNull(response.getMessage());
        });
    }

    @Test
    public void testUpdatePicNotFound() throws Exception {
        PicUpdateDto request = new PicUpdateDto();
        request.setEmail("pic1new@example.com");

        mockMvc.perform(
                put("/v1/pic/pic2")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
        ).andExpect(
                status().isNotFound()
        ).andDo(result -> {
            Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                    new TypeReference<>() {});

            assertEquals(404, response.getCode());
            assertNull(response.getData());
            assertNotNull(response.getMessage());
        });
    }

    @Test
    public void testUpdatePicSuccess() throws Exception {
        PicUpdateDto request = new PicUpdateDto();
        request.setEmail("pic1new@example.com");
        request.setFullname("Pic Dua New");
        request.setPassword("secretpass");

        mockMvc.perform(
                put("/v1/pic/pic1")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
        ).andExpect(
                status().isOk()
        ).andDo(result -> {
            Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                    new TypeReference<>() {});

            assertEquals(200, response.getCode());
            assertNotNull(response.getData());
            assertEquals("Update pic data success", response.getMessage());
        });
    }

    @Test
    public void testDeletePicNotFound() throws Exception {
        mockMvc.perform(
                patch("/v1/pic/pic2")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(
                status().isNotFound()
        ).andDo(result -> {
            Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                    new TypeReference<>() {});

            assertEquals(404, response.getCode());
            assertNull(response.getData());
            assertNotNull(response.getMessage());
        });
    }

    @Test
    public void testDeletePicSuccess() throws Exception {
        mockMvc.perform(
                patch("/v1/pic/pic1")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(
                status().isOk()
        ).andDo(result -> {
            Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                    new TypeReference<>() {});

            assertEquals(200, response.getCode());
            assertNotNull(response.getData());
            assertEquals("Delete pic data success", response.getMessage());
        });
    }
}
