package com.clientless.usersplatform.controller;

import com.clientless.usersplatform.UserApplication;
import com.clientless.usersplatform.domain.UserDomain;
import com.clientless.usersplatform.dto.user.UserRegisterDto;
import com.clientless.usersplatform.dto.user.UserUpdateDto;
import com.clientless.usersplatform.model.Response;
import com.clientless.usersplatform.repository.UserRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = UserApplication.class)
@AutoConfigureMockMvc
class UserDomainControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        userRepository.deleteAll();

        UserDomain userDomain = new UserDomain();
        userDomain.setEmail("user1@example.com");
        userDomain.setUsername("user1");
        userDomain.setPassword("rahasia");
        userDomain.setFullname("User Satu");
        userDomain.setStatus(true);

        userRepository.save(userDomain);
    }

    @AfterEach
    void tearDown() {
        userRepository.deleteAll();
    }

    @Test
    public void testRegisterUserBadRequest() throws Exception {
        // Blank email & password
        UserRegisterDto request = new UserRegisterDto();
        request.setEmail("");
        request.setPassword("");

        mockMvc.perform(
                post("/v1/user/signup")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
        ).andExpect(
                status().isBadRequest()
        ).andDo(result -> {
            Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                    new TypeReference<>() {});

            assertEquals(400, response.getCode());
            assertNull(response.getData());
            assertNotNull(response.getMessage());
        });

        // Wrong format email
        request = new UserRegisterDto();
        request.setEmail("wrong-format");
        request.setPassword("rahasia");

        mockMvc.perform(
                post("/v1/user/signup")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
        ).andExpect(
                status().isBadRequest()
        ).andDo(result -> {
            Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                    new TypeReference<>() {});

            assertEquals(400, response.getCode());
            assertNull(response.getData());
            assertNotNull(response.getMessage());
        });
    }

    @Test
    public void testRegisterUsedEmail() throws Exception {
        UserRegisterDto request = new UserRegisterDto();
        request.setEmail("user1@example.com");
        request.setPassword("secret");
        request.setFullname("User Satu");

        mockMvc.perform(
                post("/v1/user/signup")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
        ).andExpect(
                status().isImUsed()
        ).andDo(result -> {
            Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                    new TypeReference<>() {});

            assertEquals(226, response.getCode());
            assertNull(response.getData());
            assertNotNull(response.getMessage());
        });
    }

    @Test
    public void testRegisterUserSuccess() throws Exception {
        UserRegisterDto request = new UserRegisterDto();
        request.setEmail("user2@example.com");
        request.setPassword("secret");
        request.setFullname("User Dua");

        mockMvc.perform(
                post("/v1/user/signup")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
        ).andExpect(
                status().isCreated()
        ).andDo(result -> {
            Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                    new TypeReference<>() {});

            assertEquals(201, response.getCode());
            assertNotNull(response.getData());
            assertEquals("Registration success", response.getMessage());
        });
    }

    @Test
    public void testGetUserNotFound() throws Exception {
        mockMvc.perform(
                get("/v1/user/salah")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(
                status().isNotFound()
        ).andDo(result -> {
            Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                    new TypeReference<>() {});

            assertEquals(404, response.getCode());
            assertNull(response.getData());
            assertNotNull(response.getMessage());
        });
    }

    @Test
    public void testGetUserSuccess() throws Exception {
        mockMvc.perform(
                get("/v1/user/user1")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(
                status().isOk()
        ).andDo(result -> {
            Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                    new TypeReference<>() {});

            assertEquals(200, response.getCode());
            assertNotNull(response.getData());
            assertEquals("Data user found", response.getMessage());
        });
    }

    @Test
    public void testUpdateUserBadRequest() throws Exception {
        // wrong email pattern
        UserUpdateDto request = new UserUpdateDto();
        request.setEmail("wrong-format");
        request.setPassword("rahasia");

        mockMvc.perform(
                put("/v1/user/user1")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
        ).andExpect(
                status().isBadRequest()
        ).andDo(result -> {
            Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                    new TypeReference<>() {});

            assertEquals(400, response.getCode());
            assertNull(response.getData());
            assertNotNull(response.getMessage());
        });
    }

    @Test
    public void testUpdateUserNotFound() throws Exception {
        UserUpdateDto request = new UserUpdateDto();
        request.setEmail("user2new@example.com");
        request.setPassword("secret");
        request.setFullname("User Dua Baru");

        mockMvc.perform(
                put("/v1/user/salah")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
        ).andExpect(
                status().isNotFound()
        ).andDo(result -> {
            Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                    new TypeReference<>() {});

            assertEquals(404, response.getCode());
            assertNull(response.getData());
            assertNotNull(response.getMessage());
        });
    }

    @Test
    public void testUpdateUserSuccess() throws Exception {
        UserUpdateDto request = new UserUpdateDto();
        request.setEmail("user1new@example.com");
        request.setPassword("secret");
        request.setFullname("User Satu Baru");

        mockMvc.perform(
                put("/v1/user/user1")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
        ).andExpect(
                status().isOk()
        ).andDo(result -> {
            Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                    new TypeReference<>() {});

            assertEquals(200, response.getCode());
            assertNotNull(response.getData());
            assertEquals("Update user data success", response.getMessage());
        });
    }

    @Test
    public void testDeleteUserNotFound() throws Exception {
        mockMvc.perform(
                patch("/v1/user/salah")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(
                status().isNotFound()
        ).andDo(result -> {
            Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                    new TypeReference<>() {});

            assertEquals(404, response.getCode());
            assertNull(response.getData());
            assertNotNull(response.getMessage());
        });
    }

    @Test
    public void testDeleteUserSuccess() throws Exception {
        mockMvc.perform(
                patch("/v1/user/user1")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(
                status().isOk()
        ).andDo(result -> {
            Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                    new TypeReference<>() {});

            assertEquals(200, response.getCode());
            assertNotNull(response.getData());
            assertEquals("Delete user data success", response.getMessage());
        });
    }
}
