package com.clientless.usersplatform.controller;

import com.clientless.usersplatform.UserApplication;
import com.clientless.usersplatform.domain.AdminDomain;
import com.clientless.usersplatform.dto.admin.AdminRegisterDto;
import com.clientless.usersplatform.dto.admin.AdminUpdateDto;
import com.clientless.usersplatform.model.Response;
import com.clientless.usersplatform.repository.AdminRepository;
import com.clientless.usersplatform.security.BCrypt;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = UserApplication.class)
@AutoConfigureMockMvc
class AdminDomainControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private AdminRepository adminRepository;
    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        adminRepository.deleteAll();

        AdminDomain adminDomain = new AdminDomain();
        adminDomain.setEmail("admin1@example.com");
        adminDomain.setUsername("admin1");
        adminDomain.setFullname("Admin Satu");
        adminDomain.setPassword(BCrypt.hashpw("rahasia", BCrypt.gensalt()));
        adminRepository.save(adminDomain);
    }

    @AfterEach
    void tearDown() {
        adminRepository.deleteAll();
    }

    @Test
    public void testRegisterBadRequest() throws Exception {
        // Blank email & password
        AdminRegisterDto request = new AdminRegisterDto();
        request.setEmail("");
        request.setPassword("");

        mockMvc.perform(
                post("/v1/admin/signup")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
        ).andExpect(
                status().isBadRequest()
        ).andDo(result -> {
            Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                    new TypeReference<>() {});

            assertEquals(400, response.getCode());
            assertNull(response.getData());
            assertNotNull(response.getMessage());
        });

        // Wrong format email
        request = new AdminRegisterDto();
        request.setEmail("wrong-format");
        request.setPassword("rahasia");

        mockMvc.perform(
                post("/v1/admin/signup")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
        ).andExpect(
                status().isBadRequest()
        ).andDo(result -> {
            Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                    new TypeReference<>() {});

            assertEquals(400, response.getCode());
            assertNull(response.getData());
            assertNotNull(response.getMessage());
        });
    }

    @Test
    public void testRegisterUsedEmail() throws Exception {
        // Using email from BeforeEach
        AdminRegisterDto request = new AdminRegisterDto();
        request.setEmail("admin1@example.com");
        request.setPassword("rahasia");

        mockMvc.perform(
                post("/v1/admin/signup")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
        ).andExpect(
                status().isImUsed()
        ).andDo(result -> {
            Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                    new TypeReference<>() {});

            assertEquals(226, response.getCode());
            assertNull(response.getData());
            assertNotNull(response.getMessage());
        });
    }

    @Test
    public void testRegisterSuccess() throws Exception {
        AdminRegisterDto request = new AdminRegisterDto();
        request.setEmail("admin2@example.com");
        request.setPassword("secret");
        request.setFullname("Admin Dua");
        request.setEmailCompany("admin2@xxx.com");

        mockMvc.perform(
                post("/v1/admin/signup")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
        ).andExpect(
                status().isCreated()
        ).andDo(result -> {
            Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                    new TypeReference<>() {});

            assertEquals(201, response.getCode());
            assertNotNull(response.getData());
            assertEquals("Registration success", response.getMessage());
        });
    }

    @Test
    public void testGetAdminNotFound() throws Exception {
        mockMvc.perform(
                get("/v1/admin/salah")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(
                status().isNotFound()
        ).andDo(result -> {
            Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                    new TypeReference<>() {});

            assertEquals(404, response.getCode());
            assertNull(response.getData());
            assertNotNull(response.getMessage());
        });
    }

    @Test
    public void testGetAdminSuccess() throws Exception {
        mockMvc.perform(
                get("/v1/admin/admin1")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(
                status().isOk()
        ).andDo(result -> {
            Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                    new TypeReference<>() {});

            assertEquals(200, response.getCode());
            assertNotNull(response.getData());
            assertEquals("Data found", response.getMessage());
        });
    }

    @Test
    public void testUpdateBadRequest() throws Exception {
        // wrong-pattern email
        AdminUpdateDto request = new AdminUpdateDto();
        request.setEmail("wrong-format");

        mockMvc.perform(
                put("/v1/admin/admin1")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
        ).andExpect(
                status().isBadRequest()
        ).andDo(result -> {
            Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                    new TypeReference<>() {});

            assertEquals(400, response.getCode());
            assertNull(response.getData());
            assertNotNull(response.getMessage());
        });
    }

    @Test
    public void testUpdateNotFound() throws Exception {
        AdminUpdateDto request = new AdminUpdateDto();
        request.setEmail("admin1new@example.com");
        request.setPassword("secretpass");
        request.setFullname("Admin Satu Baru");
        request.setEmailCompany("admin1new@xxx.com");

        mockMvc.perform(
                put("/v1/admin/salah")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
        ).andExpect(
                status().isNotFound()
        ).andDo(result -> {
            Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                    new TypeReference<>() {});

            assertEquals(404, response.getCode());
            assertNull(response.getData());
            assertNotNull(response.getMessage());
        });
    }

    @Test
    public void testUpdateSuccess() throws Exception {
        AdminUpdateDto request = new AdminUpdateDto();
        request.setEmail("admin1new@example.com");
        request.setPassword("secretpass");
        request.setFullname("Admin Satu Baru");
        request.setEmailCompany("admin1new@xxx.com");

        mockMvc.perform(
                put("/v1/admin/admin1")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(request))
        ).andExpect(
                status().isOk()
        ).andDo(result -> {
            Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                    new TypeReference<>() {});

            assertEquals(200, response.getCode());
            assertNotNull(response.getData());
            assertEquals("Update success", response.getMessage());
        });
    }

    @Test
    public void testDeleteAdminNotFound() throws Exception {
        mockMvc.perform(
                patch("/v1/admin/salah")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(
                status().isNotFound()
        ).andDo(result -> {
            Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                    new TypeReference<>() {});

            assertEquals(404, response.getCode());
            assertNull(response.getData());
            assertNotNull(response.getMessage());
        });
    }

    @Test
    public void testDeleteAdminSuccess() throws Exception {
        mockMvc.perform(
                patch("/v1/admin/admin1")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(
                status().isOk()
        ).andDo(result -> {
            Response response = objectMapper.readValue(result.getResponse().getContentAsString(),
                    new TypeReference<>() {});

            assertEquals(200, response.getCode());
            assertNotNull(response.getData());
            assertEquals("Delete data success", response.getMessage());
        });
    }
}
