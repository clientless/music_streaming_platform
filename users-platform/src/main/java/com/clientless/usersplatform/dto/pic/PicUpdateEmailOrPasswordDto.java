package com.clientless.usersplatform.dto.pic;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PicUpdateEmailOrPasswordDto {
    @Email
    private String email;
    private String password;
}
