package com.clientless.usersplatform.dto.admin;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdminUpdateEmailOrPasswordDto {
    @Email
    private String email;
    private String password;
}
