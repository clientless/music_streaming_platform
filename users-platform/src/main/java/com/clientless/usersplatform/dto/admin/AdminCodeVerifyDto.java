package com.clientless.usersplatform.dto.admin;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdminCodeVerifyDto {
    private String codeAccess;
}
