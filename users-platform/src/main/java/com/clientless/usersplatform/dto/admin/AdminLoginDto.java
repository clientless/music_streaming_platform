package com.clientless.usersplatform.dto.admin;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdminLoginDto {
    private String username;
    @Email
    private String emailCompany;
    @NotBlank
    private String password;
}
