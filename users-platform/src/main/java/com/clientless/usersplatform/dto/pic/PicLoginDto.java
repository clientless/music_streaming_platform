package com.clientless.usersplatform.dto.pic;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PicLoginDto {
    private String username;
    @NotBlank
    private String password;
}
