package com.clientless.usersplatform.dto.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserUpdateDto {
    @Email
    private String email;
    private String password;
    private String fullname;
}
