package com.clientless.usersplatform.dto.pic;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PicRegisterDto {
    @NotBlank
    @Email
    private String email;
    @NotBlank
    private String password;
    private String fullname;
}
