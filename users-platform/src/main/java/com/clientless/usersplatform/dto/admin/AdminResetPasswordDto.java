package com.clientless.usersplatform.dto.admin;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdminResetPasswordDto {
    @Email
    private String emailCompany;
    private String password;
}
