package com.clientless.usersplatform.filter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.clientless.usersplatform.config.AuthenticationConfigConstants;
import com.clientless.usersplatform.domain.AdminDomain;
import com.clientless.usersplatform.domain.PicDomain;
import com.clientless.usersplatform.domain.UserDomain;
import com.clientless.usersplatform.dto.LoginMapper;
import com.clientless.usersplatform.exception.UnauthorizedCustomExceptionJWT;
import com.clientless.usersplatform.repository.AdminRepository;
import com.clientless.usersplatform.repository.PicRepository;
import com.clientless.usersplatform.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.SneakyThrows;
import org.json.JSONObject;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class JWTAuthenticationFilterLogin extends UsernamePasswordAuthenticationFilter {


    private final AuthenticationManager authenticationManager;
    private final AdminRepository adminRepository;
    private final PicRepository picRepository;
    private final UserRepository userRepository;


    public JWTAuthenticationFilterLogin(AuthenticationManager authenticationManager, AdminRepository adminRepository, PicRepository picRepository, UserRepository userRepository) {
        this.authenticationManager = authenticationManager;
        this.adminRepository = adminRepository;
        this.picRepository = picRepository;
        this.userRepository = userRepository;
    }


    @SneakyThrows
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
        try {
            LoginMapper creds = new ObjectMapper().readValue(request.getInputStream(), LoginMapper.class);
            UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(
                    creds.getUsername(),
                    creds.getPassword(),
                    new ArrayList<>()
            );
            return authenticationManager.authenticate(authRequest);

        } catch (AuthenticationException | IOException ex) {
            assert ex instanceof AuthenticationException;
            handleAuthenticationException(response, (AuthenticationException) ex);
        }
        return null;
    }

    private void handleAuthenticationException(HttpServletResponse response, AuthenticationException ex) throws IOException {
        String errorMessage = ex.getMessage();
        UnauthorizedCustomExceptionJWT unauthorizedCustomExceptionJWT = new UnauthorizedCustomExceptionJWT();

        try {
            unauthorizedCustomExceptionJWT.generateError(response, errorMessage);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication auth) throws IOException {
        AdminDomain adminDomain = adminRepository.findByUsernameAndStatus(((User) auth.getPrincipal()).getUsername(), true);
        String errMsg = "";
        if (adminDomain == null && ((User) auth.getPrincipal()).getUsername().contains("ADM-MSP")) {
            errMsg += "User Not Found in Admin Data";
            logger.warn(errMsg);
        }
        PicDomain picDomain = picRepository.findByUsernameAndStatus(((User) auth.getPrincipal()).getUsername(), true);
        if (picDomain == null && ((User) auth.getPrincipal()).getUsername().contains("PIC-MSP")) {
            errMsg += "User Not Found in Pic Data";
            logger.warn(errMsg);
        }
        UserDomain userDomain = userRepository.findByUsernameAndStatus(((User) auth.getPrincipal()).getUsername(), true);
        if (userDomain == null && ((User) auth.getPrincipal()).getUsername().contains("USER-MSP")) {
            errMsg += "User Not Found in User Data";
            logger.warn(errMsg);
        }
        if (adminDomain != null || picDomain != null || userDomain != null) {
            String token = JWT.create()
                    .withSubject(((User) auth.getPrincipal()).getUsername())
                    .withClaim("role", auth.getAuthorities().iterator().next().getAuthority())
                    .withExpiresAt(new Date(System.currentTimeMillis() + AuthenticationConfigConstants.EXPIRATION_TIME))
                    .sign(Algorithm.HMAC512(AuthenticationConfigConstants.SECRET.getBytes()));

            StringBuilder role = new StringBuilder();
            if(adminDomain != null){
                role.append("ADMIN");
            } else if (picDomain != null) {
                role.append("PIC");
            } else {
                role.append("USER");
            }
            JSONObject loginDataResponse = new JSONObject();
            loginDataResponse.put("username", ((User) auth.getPrincipal()).getUsername());
            loginDataResponse.put("role", role);

            JSONObject loginTokenResponse = new JSONObject();
            loginTokenResponse.put(AuthenticationConfigConstants.HEADER_STRING, AuthenticationConfigConstants.TOKEN_PREFIX + token);

            JSONObject loginResponse = new JSONObject();
            loginResponse.put("info", loginDataResponse);
            loginResponse.put("token", loginTokenResponse);


            //START - SENDING JWT AS A BODY
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(loginResponse.toString());
            //END - SENDING JWT AS A BODY

            //START - SENDING JWT AS A HEADER
            response.addHeader(AuthenticationConfigConstants.HEADER_STRING, AuthenticationConfigConstants.TOKEN_PREFIX + token);
            //END - SENDING JWT AS A HEADER
        } else {
            logger.warn(errMsg);
            JSONObject loginDataResponse = new JSONObject();
            loginDataResponse.put("username", ((User) auth.getPrincipal()).getUsername());
            loginDataResponse.put("message", errMsg);

            JSONObject loginTokenResponse = new JSONObject();
            loginTokenResponse.put("", "");

            JSONObject loginResponse = new JSONObject();
            loginResponse.put("info", loginDataResponse);
            loginResponse.put("token", loginTokenResponse);


            //START - SENDING JWT AS A BODY
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().write(loginResponse.toString());
            //END - SENDING JWT AS A BODY

            //START - SENDING JWT AS A HEADER
//            response.addHeader(AuthenticationConfigConstants.HEADER_STRING, AuthenticationConfigConstants.TOKEN_PREFIX + token);
            //END - SENDING JWT AS A HEADER
        }

    }
}
