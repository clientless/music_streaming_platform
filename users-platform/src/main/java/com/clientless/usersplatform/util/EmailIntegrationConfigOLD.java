//package com.clientless.usersplatform.util;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.integration.dsl.IntegrationFlow;
//import org.springframework.integration.dsl.IntegrationFlows;
//import org.springframework.integration.dsl.MessageChannels;
//import org.springframework.integration.mail.dsl.Mail;
//
//import javax.mail.BodyPart;
//import javax.mail.MessagingException;
//import javax.mail.Multipart;
//import javax.mail.internet.MimeMessage;
//import java.io.IOException;
//
//@Configuration
//public class EmailIntegrationConfigOLD {
//
//    @Bean
//    public IntegrationFlow emailFlow() {
//        return IntegrationFlows.from(
//                        Mail.imapIdleAdapter(imapUrl())
//                                .autoStartup(true)
//                                .javaMailProperties(p -> p.put("mail.debug", "true"))
//                                .get()
//                )
//                .channel(MessageChannels.queue("emailChannel"))
//                .handle(message -> {
//                    try {
//                        MimeMessage mimeMessage = (MimeMessage) message;
//                        handleEmail(mimeMessage);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                })
//                .get();
//    }
//
//    private void handleEmail(MimeMessage mimeMessage) {
//        try {
//            String subject = mimeMessage.getSubject();
//            System.out.println("Subject: " + subject);
//
//            Object contentObject = mimeMessage.getContent();
//            if (contentObject instanceof Multipart) {
//                handleMultipart((Multipart) contentObject);
//            }
//        } catch (MessagingException | IOException e) {
//            e.printStackTrace();
//        }
//
//        System.out.println("Received email: " + mimeMessage);
//    }
//
//    private void handleMultipart(Multipart multipart) throws MessagingException, IOException {
//        for (int i = 0; i < multipart.getCount(); i++) {
//            BodyPart bodyPart = multipart.getBodyPart(i);
//            String contentType = bodyPart.getContentType();
//
//            if (contentType.contains("text/plain")) {
//                handlePlainText(bodyPart);
//            } else if (contentType.contains("text/html")) {
//                handleHtmlContent(bodyPart);
//            }
//            // Anda dapat menambahkan logika untuk jenis konten lain jika diperlukan
//        }
//    }
//
//    private void handlePlainText(BodyPart bodyPart) throws MessagingException, IOException {
//        String textContent = (String) bodyPart.getContent();
//        System.out.println("Isi pesan (plain text): " + textContent);
//    }
//
//    private void handleHtmlContent(BodyPart bodyPart) throws MessagingException, IOException {
//        String htmlContent = (String) bodyPart.getContent();
//        System.out.println("Isi pesan (HTML): " + htmlContent);
//    }
//
//    private String imapUrl() {
//        // Sesuaikan dengan konfigurasi IMAP server Anda
//        return "imaps://no-reply.msp%40outlook.com:1234lupa@outlook.office365.com:993/INBOX";
//    }
//}
