package com.clientless.usersplatform.util;

import org.springframework.beans.factory.annotation.Value;

public class EmailFrom {
    @Value("${spring.mail.username}")
    public String mailFrom;
}
