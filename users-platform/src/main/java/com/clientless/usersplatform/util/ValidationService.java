package com.clientless.usersplatform.util;

import com.clientless.usersplatform.service.impl.AdminServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.Set;

@Service
public class ValidationService {
    public static final Logger logger = LoggerFactory.getLogger(AdminServiceImpl.class);
    private final Validator validator;

    public ValidationService(Validator validator) {
        this.validator = validator;
    }

    public void validate(Object request) {
        Set<ConstraintViolation<Object>> constraintViolations = validator.validate(request);
        if (!constraintViolations.isEmpty()) {
            logger.error("Validate request");
            throw new ConstraintViolationException(constraintViolations);
        }

    }
}
