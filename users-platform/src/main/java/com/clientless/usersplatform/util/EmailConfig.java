package com.clientless.usersplatform.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Configuration
public class EmailConfig {
    @Value("${spring.mail.host}")
    private String hostMail;

    @Value("${spring.mail.port}")
    private int portMail;  // Change data type to int if the port is an integer

    @Value("${spring.mail.username}")
    private String uNameMail;

    @Value("${spring.mail.password}")
    private String passMail;

    @Bean
    public JavaMailSender javaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(hostMail);
        mailSender.setPort(portMail); // Your SMTP port
        mailSender.setUsername("no-reply.msp-v1@outlook.com");
        mailSender.setPassword("1234lupa");

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");

        return mailSender;
    }
}
