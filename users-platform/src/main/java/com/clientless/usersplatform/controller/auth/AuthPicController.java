package com.clientless.usersplatform.controller.auth;

import com.clientless.usersplatform.dto.pic.PicRegisterDto;
import com.clientless.usersplatform.dto.pic.PicResetPasswordDto;
import com.clientless.usersplatform.model.Response;
import com.clientless.usersplatform.service.PicService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.io.IOException;

@CrossOrigin(origins="http://localhost:8082")
@RestController
@RequestMapping("/v1/auth/pic")
public class AuthPicController {
    private final PicService picService;

    public AuthPicController(PicService picService) {
        this.picService = picService;
    }

    @PostMapping(value = "/signup", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> register(@RequestBody PicRegisterDto request) throws IOException, MessagingException {
        return picService.register(request);
    }

    @PutMapping(value = "/reset", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> resetPassword(@RequestBody PicResetPasswordDto request) throws IOException, MessagingException {
        return picService.resetPassword(request);
    }

}
