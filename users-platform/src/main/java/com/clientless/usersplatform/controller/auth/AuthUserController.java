package com.clientless.usersplatform.controller.auth;

import com.clientless.usersplatform.dto.user.UserRegisterDto;
import com.clientless.usersplatform.dto.user.UserResetPasswordDto;
import com.clientless.usersplatform.model.Response;
import com.clientless.usersplatform.service.UserService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.io.IOException;

@CrossOrigin(origins="http://localhost:8082")
@RestController
@RequestMapping("/v1/auth/user")
public class AuthUserController {
    private final UserService userService;

    public AuthUserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(
            value = "/signup",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<Response> register(@RequestBody UserRegisterDto request) throws IOException, MessagingException {
        return userService.register(request);
    }

    @PutMapping(value = "/reset", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> resetPassword(@RequestBody UserResetPasswordDto request) throws IOException, MessagingException {
        return userService.resetPassword(request);
    }


}
