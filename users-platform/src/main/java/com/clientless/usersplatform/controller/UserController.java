package com.clientless.usersplatform.controller;

import com.clientless.usersplatform.dto.user.UserUpdateDto;
import com.clientless.usersplatform.dto.user.UserUpdateEmailOrPasswordDto;
import com.clientless.usersplatform.model.Response;
import com.clientless.usersplatform.service.UserService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.io.IOException;

@CrossOrigin(origins="http://localhost:8082")
@RestController
@RequestMapping("/v1/user")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping(value = "/{username}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> get(@PathVariable("username") String username) throws IOException {
        return userService.get(username);
    }

    @PutMapping(
            value = "/{username}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<Response> update(@PathVariable("username") String username,
                                           @RequestBody UserUpdateDto request) throws IOException {
        return userService.update(username, request);
    }

    @PatchMapping(value = "/{username}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> delete(@PathVariable("username") String username) throws IOException {
        return userService.delete(username);
    }


    @PutMapping(
            value = "/update/{username}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<Response> updateEmailOrPassword(@PathVariable("username") String username,
                                                          @RequestBody UserUpdateEmailOrPasswordDto request) throws IOException, MessagingException {
        return userService.updateEmailOrPassword(username, request);
    }
    @GetMapping(value = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> getList() throws IOException {
        return userService.getList();
    }


}
