package com.clientless.usersplatform.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.InetAddress;
import java.net.UnknownHostException;
@CrossOrigin(origins="http://localhost:8082")
@RestController
@RequestMapping("/v1/tc")
public class TestConnectionController {
    @GetMapping
    public String checkConnection() {
        if (isInternetAvailable()) {
            return "Connected to the internet!";
        } else {
            return "Not connected to the internet.";
        }
    }

    private boolean isInternetAvailable() {
        try {
            InetAddress.getByName("www.google.com");
            return true;
        } catch (UnknownHostException e) {
            return false;
        }
    }

}
