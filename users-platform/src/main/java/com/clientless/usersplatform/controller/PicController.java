package com.clientless.usersplatform.controller;

import com.clientless.usersplatform.dto.pic.PicUpdateDto;
import com.clientless.usersplatform.dto.pic.PicUpdateEmailOrPasswordDto;
import com.clientless.usersplatform.model.Response;
import com.clientless.usersplatform.service.PicService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.io.IOException;

@CrossOrigin(origins="http://localhost:8082")
@RestController
@RequestMapping("/v1/pic")
public class PicController {
    private final PicService picService;

    public PicController(PicService picService) {
        this.picService = picService;
    }


    @GetMapping(value = "/{username}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> get(@PathVariable("username") String username) throws IOException {
        return picService.get(username);
    }

    @PutMapping(value = "/{username}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> update(@PathVariable("username") String username,
                                           @RequestBody PicUpdateDto request) throws IOException {
        return picService.update(username, request);
    }

    @PutMapping(value = "/update/{username}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> updateEmailOrPassword(@PathVariable("username") String username,
                                                          @RequestBody PicUpdateEmailOrPasswordDto request) throws IOException, MessagingException {
        return picService.updateEmailOrPassword(username, request);
    }


    @PatchMapping(value = "/{username}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> delete(@PathVariable("username") String username) throws IOException {
        return picService.delete(username);
    }
    @GetMapping(value = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> getList() throws IOException {
        return picService.getList();
    }

}
