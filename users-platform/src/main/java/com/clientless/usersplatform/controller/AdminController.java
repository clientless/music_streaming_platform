package com.clientless.usersplatform.controller;

import com.clientless.usersplatform.dto.admin.AdminUpdateDto;
import com.clientless.usersplatform.dto.admin.AdminUpdateEmailOrPasswordDto;
import com.clientless.usersplatform.model.Response;
import com.clientless.usersplatform.service.AdminService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.io.IOException;

@CrossOrigin(origins="http://localhost:8082")
@RestController
@RequestMapping("/v1/admin")
public class AdminController {
    private final AdminService adminService;

    public AdminController(AdminService adminService) {
        this.adminService = adminService;
    }

    @GetMapping(value = "/{username}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> get(@PathVariable("username") String username) throws IOException {
        return adminService.get(username);
    }

    @PutMapping(value = "/{username}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> update(@PathVariable("username") String username,
                                           @RequestBody AdminUpdateDto request) throws IOException {
        return adminService.update(username, request);
    }

    @PutMapping(value = "/update/{username}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> updateEmailOrPassword(@PathVariable("username") String username,
                                                          @RequestBody AdminUpdateEmailOrPasswordDto request) throws IOException, MessagingException {
        return adminService.updateEmailOrPassword(username, request);
    }


    @PatchMapping(value = "/{username}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> delete(@PathVariable("username") String username) throws IOException {
        return adminService.delete(username);
    }

    @GetMapping(value = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> getList() throws IOException {
        return adminService.getList();
    }
}
