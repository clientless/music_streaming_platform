package com.clientless.usersplatform.controller.auth;

import com.clientless.usersplatform.dto.admin.AdminCodeVerifyDto;
import com.clientless.usersplatform.dto.admin.AdminRegisterDto;
import com.clientless.usersplatform.dto.admin.AdminResetPasswordDto;
import com.clientless.usersplatform.model.Response;
import com.clientless.usersplatform.service.AdminService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.io.IOException;

@CrossOrigin(origins="http://localhost:8082")
@RestController
@RequestMapping("/v1/auth/admin")
public class AuthAdminController {
    private final AdminService adminService;

    public AuthAdminController(AdminService adminService) {
        this.adminService = adminService;
    }

    @PostMapping(value = "/verify-code", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> verifyCode(@RequestBody AdminCodeVerifyDto request) throws IOException, MessagingException {
        return adminService.verifyCode(request);
    }
    @PostMapping(value = "/signup", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> register(@RequestBody AdminRegisterDto request) throws IOException, MessagingException {
        return adminService.register(request);
    }

    @PutMapping(value = "/reset", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> resetPassword(@RequestBody AdminResetPasswordDto request) throws IOException, MessagingException {
        return adminService.resetPassword(request);
    }


}
