package com.clientless.usersplatform.repository;

import com.clientless.usersplatform.domain.PicDomain;
import com.clientless.usersplatform.domain.UserDomain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserDomain, Long> {
    UserDomain findByUsername(String username);

    UserDomain findByEmail(String email);

    UserDomain findByUsernameAndStatus(String username, boolean status);

    UserDomain findByEmailAndStatus(String email, Boolean status);
}
