package com.clientless.usersplatform.repository;


import com.clientless.usersplatform.domain.CodeVerifyAdminDomain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CodeVerifyAdminRepository extends JpaRepository<CodeVerifyAdminDomain, Long> {
    CodeVerifyAdminDomain findCodeVerifyAdminDomainByCodeAccessAndCodeAccessWasActivated(String codeAccess, boolean codeAccessWasActivated);
}
