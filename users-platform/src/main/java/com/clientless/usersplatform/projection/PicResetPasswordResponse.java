package com.clientless.usersplatform.projection;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PicResetPasswordResponse {
    private String email;
    @JsonIgnore
    private String password;

}
