package com.clientless.usersplatform.service.impl.handler;

import com.clientless.usersplatform.security.BCrypt;

public class UserPasswordHandler {
    public boolean verifyPassword(String password, String passwordDB) {
        System.out.println(password);
        System.out.println(passwordDB);
        return BCrypt.checkpw(password, passwordDB);
    }

}
