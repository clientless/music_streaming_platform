package com.clientless.usersplatform.service.impl.handler.mimeMeesage;

import com.clientless.usersplatform.domain.UserDomain;
import com.clientless.usersplatform.dto.user.UserRegisterDto;
import com.clientless.usersplatform.dto.user.UserResetPasswordDto;
import com.clientless.usersplatform.dto.user.UserUpdateEmailOrPasswordDto;
import com.clientless.usersplatform.util.emailTemplate.ReadTemplate;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

public class UserMimeMessage {
    ReadTemplate readTemplate = new ReadTemplate();

    public MimeMessage createMimeMessagesUserRegistered(UserRegisterDto request, UserDomain userDomain, JavaMailSender mailSender) throws MessagingException {
        MimeMessage mimeMessage = mailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "utf-8");
        helper.setFrom("no-reply.msp-v1@outlook.com");
        helper.setTo(userDomain.getEmail());
        helper.setSubject("Your Username and Company Mail");

        // Baca isi template HTML dan CSS
        String htmlTemplatePath = "users-platform/src/main/java/com/clientless/usersplatform/util/emailTemplate/EmailTemplate.html";
        String cssFilePath = "users-platform/src/main/java/com/clientless/usersplatform/util/emailTemplate/EmailStyle.css";
        String htmlTemplate = readTemplate.readHtmlTemplate(htmlTemplatePath);
        String cssContent = readTemplate.readHtmlTemplate(cssFilePath);

        // Sisipkan CSS ke dalam template HTML
        String emailContent = htmlTemplate.replace("<style>", "<style>\n" + cssContent + "\n");
        if (!userDomain.getUsername().contains("ADM-MSP")) {
            emailContent = emailContent.replace("${typeMail}", "Mail");
        } else {
            emailContent = emailContent.replace("${typeMail}", "Work Mail");
        }

        //header message html
        emailContent = emailContent.replace("${messageHeaderHtml}", "Your registration is complete.");


        // Ganti placeholder dengan nilai yang sesuai dari objek picDomain
        emailContent = emailContent.replace("${usernameForHtml}", userDomain.getUsername());
        emailContent = emailContent.replace("${mailForHtml}", userDomain.getEmail());
        emailContent = emailContent.replace("${passwordForHtml}", request.getPassword());
        emailContent = emailContent.replace("${roleForHtml}", userDomain.getRole());

        // Setel isi email dari template
        helper.setText(emailContent, true);

        // Menyertakan inline attachment untuk logo
        ClassPathResource logoResource = new ClassPathResource("ToRing-Msp.png");
        helper.addInline("logo", logoResource);
        return mimeMessage;
    }

    public MimeMessage createMimeMessageUserUpdateEmailOrPassword(UserUpdateEmailOrPasswordDto request, UserDomain userDomain, JavaMailSender mailSender) throws MessagingException {
        MimeMessage mimeMessage = mailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "utf-8");
        helper.setFrom("no-reply.msp-v1@outlook.com");
        helper.setTo(userDomain.getEmail());
        helper.setSubject("[Changed] Your Username and Company Mail");

        // Baca isi template HTML dan CSS
        String htmlTemplatePath = "users-platform/src/main/java/com/clientless/usersplatform/util/emailTemplate/EmailTemplate.html";
        String cssFilePath = "users-platform/src/main/java/com/clientless/usersplatform/util/emailTemplate/EmailStyle.css";
        String htmlTemplate = readTemplate.readHtmlTemplate(htmlTemplatePath);
        String cssContent = readTemplate.readHtmlTemplate(cssFilePath);

        // Sisipkan CSS ke dalam template HTML
        String emailContent = htmlTemplate.replace("<style>", "<style>\n" + cssContent + "\n");
        if (!userDomain.getUsername().contains("ADM-MSP")) {
            emailContent = emailContent.replace("${typeMail}", "Mail");
        } else {
            emailContent = emailContent.replace("${typeMail}", "Work Mail");
        }

        //header message html
        emailContent = emailContent.replace("${messageHeaderHtml}", "Your data was successfully updated.");


        // Ganti placeholder dengan nilai yang sesuai dari objek picDomain
        emailContent = emailContent.replace("${usernameForHtml}", userDomain.getUsername());
        emailContent = emailContent.replace("${mailForHtml}", userDomain.getEmail());
        emailContent = emailContent.replace("${passwordForHtml}", request.getPassword());
        emailContent = emailContent.replace("${roleForHtml}", userDomain.getRole());

        // Setel isi email dari template
        helper.setText(emailContent, true);

        // Menyertakan inline attachment untuk logo
        ClassPathResource logoResource = new ClassPathResource("ToRing-Msp.png");
        helper.addInline("logo", logoResource);
        return mimeMessage;
    }

    public MimeMessage createMimeMessageUserResetPassword(UserResetPasswordDto request, UserDomain userDomain, JavaMailSender mailSender) throws MessagingException {
        MimeMessage mimeMessage = mailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "utf-8");
        helper.setFrom("no-reply.msp-v1@outlook.com");
        helper.setTo(userDomain.getEmail());
        helper.setSubject("[Changed] Your Username and Company Mail");

        // Baca isi template HTML dan CSS
        String htmlTemplatePath = "users-platform/src/main/java/com/clientless/usersplatform/util/emailTemplate/EmailTemplate.html";
        String cssFilePath = "users-platform/src/main/java/com/clientless/usersplatform/util/emailTemplate/EmailStyle.css";
        String htmlTemplate = readTemplate.readHtmlTemplate(htmlTemplatePath);
        String cssContent = readTemplate.readHtmlTemplate(cssFilePath);

        // Sisipkan CSS ke dalam template HTML
        String emailContent = htmlTemplate.replace("<style>", "<style>\n" + cssContent + "\n");
        if (!userDomain.getUsername().contains("ADM-MSP")) {
            emailContent = emailContent.replace("${typeMail}", "Mail");
        } else {
            emailContent = emailContent.replace("${typeMail}", "Work Mail");
        }

        //header message html
        emailContent = emailContent.replace("${messageHeaderHtml}", "Your data was successfully updated.");


        // Ganti placeholder dengan nilai yang sesuai dari objek picDomain
        emailContent = emailContent.replace("${usernameForHtml}", userDomain.getUsername());
        emailContent = emailContent.replace("${mailForHtml}", userDomain.getEmail());
        emailContent = emailContent.replace("${passwordForHtml}", request.getPassword());
        emailContent = emailContent.replace("${roleForHtml}", userDomain.getRole());

        // Setel isi email dari template
        helper.setText(emailContent, true);

        // Menyertakan inline attachment untuk logo
        ClassPathResource logoResource = new ClassPathResource("ToRing-Msp.png");
        helper.addInline("logo", logoResource);
        return mimeMessage;
    }


}
