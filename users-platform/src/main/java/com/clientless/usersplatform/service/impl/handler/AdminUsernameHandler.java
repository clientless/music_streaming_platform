package com.clientless.usersplatform.service.impl.handler;

import com.clientless.usersplatform.domain.AdminDomain;
import com.clientless.usersplatform.repository.AdminRepository;
import org.springframework.web.bind.annotation.ControllerAdvice;

import java.util.List;
import java.util.Random;

@ControllerAdvice
public class AdminUsernameHandler {
    private final AdminRepository adminRepository;

    public AdminUsernameHandler(AdminRepository adminRepository) {
        this.adminRepository = adminRepository;
    }

    public boolean IsAdminExistByEmail(String adminEmail) {
        boolean isExist = false;
        List<AdminDomain> adminDomainList = adminRepository.findAll();
        for (AdminDomain adminDomain : adminDomainList) {
            if (adminEmail.equals(adminDomain.getEmail()) && adminDomain.isStatus()) {
                isExist = true;
                break;
            }
        }
        return isExist;
    }

    public boolean isAdminExistByUsername(String username) {
        AdminDomain adminDomainCheck = adminRepository.findByUsername(username);
        return adminDomainCheck != null;
    }

    public String generateUserName(String fullName) {
        String[] words = fullName.split("[^a-zA-Z0-9']+");
        String codeAdmin = "ADM-MSP";
        String randomValue = Integer.toString(new Random().nextInt(10000));
        StringBuilder baseUsername = createBaseUsername(words, codeAdmin);
        String generatedUsername = appendRandomValue(baseUsername, randomValue);
        return isAdminExistByUsername(generatedUsername) ? generateUserName(fullName) : generatedUsername;

    }

    private StringBuilder createBaseUsername(String[] words, String codeAdmin) {
        StringBuilder baseUsername = new StringBuilder();

        if (words.length > 0) {
            baseUsername.append(words[0]);
            for (int i = 1; i < words.length; i++) {
                if (!words[i].isEmpty()) {
                    baseUsername.append(words[i].charAt(0));
                }
            }
            baseUsername.append(codeAdmin);
        }

        return baseUsername;
    }

    private String appendRandomValue(StringBuilder baseUsername, String randomValue) {
        return baseUsername.append(randomValue).toString();
    }


}
