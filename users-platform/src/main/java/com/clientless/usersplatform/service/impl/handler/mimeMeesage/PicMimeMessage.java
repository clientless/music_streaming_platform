package com.clientless.usersplatform.service.impl.handler.mimeMeesage;

import com.clientless.usersplatform.domain.PicDomain;
import com.clientless.usersplatform.dto.pic.PicRegisterDto;
import com.clientless.usersplatform.dto.pic.PicResetPasswordDto;
import com.clientless.usersplatform.dto.pic.PicUpdateEmailOrPasswordDto;
import com.clientless.usersplatform.util.emailTemplate.ReadTemplate;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

public class PicMimeMessage {
    ReadTemplate readTemplate = new ReadTemplate();

    public MimeMessage createMimeMessagesPicRegistered(PicRegisterDto request, PicDomain picDomain, JavaMailSender mailSender) throws MessagingException {
        MimeMessage mimeMessage = mailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "utf-8");
        helper.setFrom("no-reply.msp-v1@outlook.com");
        helper.setTo(picDomain.getEmail());
        helper.setSubject("Your Username and Company Mail");

        // Baca isi template HTML dan CSS
        String htmlTemplatePath = "users-platform/src/main/java/com/clientless/usersplatform/util/emailTemplate/EmailTemplate.html";
        String cssFilePath = "users-platform/src/main/java/com/clientless/usersplatform/util/emailTemplate/EmailStyle.css";
        String htmlTemplate = readTemplate.readHtmlTemplate(htmlTemplatePath);
        String cssContent = readTemplate.readHtmlTemplate(cssFilePath);

        // Sisipkan CSS ke dalam template HTML
        String emailContent = htmlTemplate.replace("<style>", "<style>\n" + cssContent + "\n");
        if (!picDomain.getUsername().contains("ADM-MSP")) {
            emailContent = emailContent.replace("${typeMail}", "Mail");
        } else {
            emailContent = emailContent.replace("${typeMail}", "Work Mail");
        }

        //header message html
        emailContent = emailContent.replace("${messageHeaderHtml}", "Your registration is complete.");


        // Ganti placeholder dengan nilai yang sesuai dari objek picDomain
        emailContent = emailContent.replace("${usernameForHtml}", picDomain.getUsername());
        emailContent = emailContent.replace("${mailForHtml}", picDomain.getEmail());
        emailContent = emailContent.replace("${passwordForHtml}", request.getPassword());
        emailContent = emailContent.replace("${roleForHtml}", picDomain.getRole());

        // Setel isi email dari template
        helper.setText(emailContent, true);

        // Menyertakan inline attachment untuk logo
        ClassPathResource logoResource = new ClassPathResource("ToRing-Msp.png");
        helper.addInline("logo", logoResource);
        return mimeMessage;
    }

    public MimeMessage createMimeMessagePicUpdateEmailOrPassword(PicUpdateEmailOrPasswordDto request, PicDomain picDomain, JavaMailSender mailSender) throws MessagingException {
        MimeMessage mimeMessage = mailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "utf-8");
        helper.setFrom("no-reply.msp-v1@outlook.com");
        helper.setTo(picDomain.getEmail());
        helper.setSubject("[Changed] Your Username and Company Mail");

        // Baca isi template HTML dan CSS
        String htmlTemplatePath = "users-platform/src/main/java/com/clientless/usersplatform/util/emailTemplate/EmailTemplate.html";
        String cssFilePath = "users-platform/src/main/java/com/clientless/usersplatform/util/emailTemplate/EmailStyle.css";
        String htmlTemplate = readTemplate.readHtmlTemplate(htmlTemplatePath);
        String cssContent = readTemplate.readHtmlTemplate(cssFilePath);

        // Sisipkan CSS ke dalam template HTML
        String emailContent = htmlTemplate.replace("<style>", "<style>\n" + cssContent + "\n");
        if (!picDomain.getUsername().contains("ADM-MSP")) {
            emailContent = emailContent.replace("${typeMail}", "Mail");
        } else {
            emailContent = emailContent.replace("${typeMail}", "Work Mail");
        }

        //header message html
        emailContent = emailContent.replace("${messageHeaderHtml}", "Your data was successfully updated.");


        // Ganti placeholder dengan nilai yang sesuai dari objek picDomain
        emailContent = emailContent.replace("${usernameForHtml}", picDomain.getUsername());
        emailContent = emailContent.replace("${mailForHtml}", picDomain.getEmail());
        emailContent = emailContent.replace("${passwordForHtml}", request.getPassword());
        emailContent = emailContent.replace("${roleForHtml}", picDomain.getRole());

        // Setel isi email dari template
        helper.setText(emailContent, true);

        // Menyertakan inline attachment untuk logo
        ClassPathResource logoResource = new ClassPathResource("ToRing-Msp.png");
        helper.addInline("logo", logoResource);
        return mimeMessage;
    }

    public MimeMessage createMimeMessagePicResetPassword(PicResetPasswordDto request, PicDomain picDomain, JavaMailSender mailSender) throws MessagingException {
        MimeMessage mimeMessage = mailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "utf-8");
        helper.setFrom("no-reply.msp-v1@outlook.com");
        helper.setTo(picDomain.getEmail());
        helper.setSubject("[Reset Password] Your Username and Company Mail");

        // Baca isi template HTML dan CSS
        String htmlTemplatePath = "users-platform/src/main/java/com/clientless/usersplatform/util/emailTemplate/EmailTemplate.html";
        String cssFilePath = "users-platform/src/main/java/com/clientless/usersplatform/util/emailTemplate/EmailStyle.css";
        String htmlTemplate = readTemplate.readHtmlTemplate(htmlTemplatePath);
        String cssContent = readTemplate.readHtmlTemplate(cssFilePath);

        // Sisipkan CSS ke dalam template HTML
        String emailContent = htmlTemplate.replace("<style>", "<style>\n" + cssContent + "\n");
        if (!picDomain.getUsername().contains("ADM-MSP")) {
            emailContent = emailContent.replace("${typeMail}", "Mail");
        } else {
            emailContent = emailContent.replace("${typeMail}", "Work Mail");
        }

        //header message html
        emailContent = emailContent.replace("${messageHeaderHtml}", "Your data was successfully updated.");


        // Ganti placeholder dengan nilai yang sesuai dari objek picDomain
        emailContent = emailContent.replace("${usernameForHtml}", picDomain.getUsername());
        emailContent = emailContent.replace("${mailForHtml}", picDomain.getEmail());
        emailContent = emailContent.replace("${passwordForHtml}", request.getPassword());
        emailContent = emailContent.replace("${roleForHtml}", picDomain.getRole());

        // Setel isi email dari template
        helper.setText(emailContent, true);

        // Menyertakan inline attachment untuk logo
        ClassPathResource logoResource = new ClassPathResource("ToRing-Msp.png");
        helper.addInline("logo", logoResource);
        return mimeMessage;
    }


}
