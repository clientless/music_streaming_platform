package com.clientless.usersplatform.service;

import com.clientless.usersplatform.dto.user.UserRegisterDto;
import com.clientless.usersplatform.dto.user.UserResetPasswordDto;
import com.clientless.usersplatform.dto.user.UserUpdateDto;
import com.clientless.usersplatform.dto.user.UserUpdateEmailOrPasswordDto;
import com.clientless.usersplatform.model.Response;
import org.springframework.http.ResponseEntity;

import javax.mail.MessagingException;
import java.io.IOException;

public interface UserService {
    ResponseEntity<Response> register(UserRegisterDto request) throws IOException, MessagingException;

    ResponseEntity<Response> get(String username) throws IOException;
    ResponseEntity<Response> getList() throws IOException;


    ResponseEntity<Response> update(String username, UserUpdateDto request) throws IOException;

    ResponseEntity<Response> updateEmailOrPassword(String username, UserUpdateEmailOrPasswordDto request) throws IOException, MessagingException;

    ResponseEntity<Response> delete(String username) throws IOException;

    ResponseEntity<Response> resetPassword(UserResetPasswordDto request) throws IOException, MessagingException;

}
