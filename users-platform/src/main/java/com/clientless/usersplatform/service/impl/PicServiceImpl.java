package com.clientless.usersplatform.service.impl;

import com.clientless.usersplatform.domain.AdminDomain;
import com.clientless.usersplatform.domain.PicDomain;
import com.clientless.usersplatform.domain.UserDomain;
import com.clientless.usersplatform.dto.pic.*;
import com.clientless.usersplatform.exception.ConflictCustomExceptionJWT;
import com.clientless.usersplatform.exception.NotFoundException;
import com.clientless.usersplatform.model.Response;
import com.clientless.usersplatform.projection.PicResetPasswordResponse;
import com.clientless.usersplatform.projection.PicUpdateEmailOrPasswordResponse;
import com.clientless.usersplatform.repository.PicRepository;
import com.clientless.usersplatform.security.BCrypt;
import com.clientless.usersplatform.service.PicService;
import com.clientless.usersplatform.service.impl.handler.PicUsernameHandler;
import com.clientless.usersplatform.service.impl.handler.mimeMeesage.PicMimeMessage;
import com.clientless.usersplatform.util.GenerateResponse;
import com.clientless.usersplatform.util.ValidationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class PicServiceImpl implements PicService {
    public static final Logger logger = LoggerFactory.getLogger(PicServiceImpl.class);
    private final PicRepository picRepository;
    private final PicUsernameHandler picUsernameHandler;
    private final ValidationService validationService;
    private final JavaMailSender javaMailSender;
    private final HttpServletResponse httpServletResponse;


    PicMimeMessage picMimeMessage = new PicMimeMessage();

    public PicServiceImpl(PicRepository picRepository, PicUsernameHandler picUsernameHandler, ValidationService validationService, JavaMailSender javaMailSender, HttpServletResponse httpServletResponse) {
        this.picRepository = picRepository;
        this.picUsernameHandler = picUsernameHandler;
        this.validationService = validationService;
        this.javaMailSender = javaMailSender;
        this.httpServletResponse = httpServletResponse;
    }

    public PicDomain readUserByUsername(String username) {
        return picRepository.findByUsername(username);
    }


    @Transactional
    @Override
    public ResponseEntity<Response> register(PicRegisterDto request) throws IOException, MessagingException {
        logger.info("Request to register: {}", request.getEmail());

        // validate email & password not blank & email-format
        validationService.validate(request);

        if (Objects.equals(request.getFullname(), "") || request.getFullname().split(" ").length == 0) {
            String msgErr = "Field Fullname Must be Filled";
            logger.error(msgErr);
            return GenerateResponse.badRequest("Bad Request", msgErr);
        }


        // check used email
        boolean isExistByEmail = picUsernameHandler.IsPicExistByEmail(request.getEmail());
        if (isExistByEmail) {
            logger.info("Checking email of: {}", request.getEmail());
            String errMsg = "Account with your email has been registered.";
            ConflictCustomExceptionJWT conflictCustomExceptionJWT = new ConflictCustomExceptionJWT();
            conflictCustomExceptionJWT.generateError(httpServletResponse, errMsg);
            return GenerateResponse.conflict("Conflict",errMsg);
        }
        String username = picUsernameHandler.generateUserName(request.getFullname());
        PicDomain picDomain = new PicDomain();
        picDomain.setEmail(request.getEmail());
        picDomain.setUsername(username);
        picDomain.setPassword(BCrypt.hashpw(request.getPassword(), BCrypt.gensalt()));
//        picDomain.setPassword(passwordEncoder.encode(request.getPassword()));
        picDomain.setFullname(request.getFullname());
        picDomain.setStatus(true);

        picRepository.save(picDomain);

        MimeMessage message = picMimeMessage.createMimeMessagesPicRegistered(request, picDomain, javaMailSender);

        javaMailSender.send(message);
        return GenerateResponse.created("Registration success", picDomain);
    }


    @Transactional
    @Override
    public ResponseEntity<Response> get(String username) throws IOException {
        logger.info("Load data of username: {}", username);

        PicDomain picDomain = picRepository.findByUsername(username);
        if (picDomain == null) {
            throw new NotFoundException(username);
        }

        return GenerateResponse.success("Data found", picDomain);
    }

    @Override
    public ResponseEntity<Response> getList() throws IOException {
        try {
            List<PicDomain> picDomainList = new ArrayList<>(picRepository.findAll());
            return GenerateResponse.success("Successfully get data", picDomainList);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return GenerateResponse.error("Something Wrong", null);
        }
    }

    @Transactional
    @Override
    public ResponseEntity<Response> update(String username, PicUpdateDto request) throws IOException {
        logger.info("Request to update: {}", username);

        // validate email pattern
        validationService.validate(request);

        PicDomain picDomain = picRepository.findByUsername(username);
        if (picDomain == null) {
            throw new NotFoundException(username);
        }

        if (!request.getEmail().isBlank() || !request.getEmail().isEmpty()) {
            picDomain.setEmail(request.getEmail());
        }
        if (!request.getFullname().isBlank() || !request.getFullname().isEmpty()) {
            picDomain.setFullname(request.getFullname());
        }
        if (!request.getPassword().isBlank() || !request.getPassword().isEmpty()) {
            picDomain.setPassword(BCrypt.hashpw(request.getPassword(), BCrypt.gensalt()));
        }

        picRepository.save(picDomain);
        return GenerateResponse.success("Update pic data success", picDomain);
    }

    @Transactional
    @Override
    public ResponseEntity<Response> updateEmailOrPassword(String username, PicUpdateEmailOrPasswordDto request) throws IOException, MessagingException {
        logger.info("Request to update: {}", username);

        // validate email in the right pattern
        validationService.validate(request);
        PicDomain picDomain = picRepository.findByUsername(username);
        if (picDomain == null) {
            return GenerateResponse.notFound("Not Found User With Username", username);
        } else {
            if (!request.getEmail().isBlank() || !request.getEmail().isEmpty()) {
                picDomain.setEmail(request.getEmail());
                if (!request.getPassword().isBlank() || !request.getPassword().isEmpty()) {
                    picDomain.setPassword(BCrypt.hashpw(request.getPassword(), BCrypt.gensalt()));
                    picRepository.save(picDomain);
                    MimeMessage message = picMimeMessage.createMimeMessagePicUpdateEmailOrPassword(request, picDomain, javaMailSender);
                    javaMailSender.send(message);
                    PicUpdateEmailOrPasswordResponse picUpdateEmailOrPasswordResponse = new PicUpdateEmailOrPasswordResponse(picDomain.getEmail(), picDomain.getPassword());
                    return GenerateResponse.success("Update success", picUpdateEmailOrPasswordResponse);
                } else {
                    return GenerateResponse.badRequest("Password must be filled", null);
                }
            } else {
                return GenerateResponse.badRequest("Email must be filled", null);
            }
        }

    }

    @Transactional
    @Override
    public ResponseEntity<Response> delete(String username) throws IOException {
        logger.info("Request to delete: {}", username);

        PicDomain picDomain = picRepository.findByUsername(username);
        if (picDomain == null) {
            throw new NotFoundException(username);
        }

        picDomain.setStatus(false);
        picRepository.save(picDomain);

        return GenerateResponse.success("Delete pic data success", picDomain);
    }

    @Override
    public ResponseEntity<Response> resetPassword(PicResetPasswordDto request) throws IOException, MessagingException {
        // validate email in the right pattern
        validationService.validate(request);

        PicDomain picDomain = picRepository.findByEmailAndStatus(request.getEmail(), true);
        if (picDomain == null) {
            return GenerateResponse.notFound("Not Found User With Email", request.getEmail());
        } else {
            if (!request.getPassword().isBlank() || !request.getPassword().isEmpty()) {
                logger.info("Request to update: {}", request.getEmail());
                picDomain.setPassword(BCrypt.hashpw(request.getPassword(), BCrypt.gensalt()));
                picRepository.save(picDomain);
                MimeMessage message = picMimeMessage.createMimeMessagePicResetPassword(request, picDomain, javaMailSender);
                javaMailSender.send(message);
                PicResetPasswordResponse picResetPasswordResponse = new PicResetPasswordResponse(picDomain.getEmail(), picDomain.getPassword());
                return GenerateResponse.success("Update success", picResetPasswordResponse);
            } else {
                return GenerateResponse.badRequest("Password Must Be Filled", null);
            }
        }

    }
}
