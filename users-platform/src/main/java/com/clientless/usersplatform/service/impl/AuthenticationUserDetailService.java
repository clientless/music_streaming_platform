package com.clientless.usersplatform.service.impl;

import com.clientless.usersplatform.domain.AdminDomain;
import com.clientless.usersplatform.domain.PicDomain;
import com.clientless.usersplatform.domain.UserDomain;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class AuthenticationUserDetailService implements UserDetailsService {

    private final AdminServiceImpl adminService;
    private final UserServiceImpl userService;
    private final PicServiceImpl picService;

    public AuthenticationUserDetailService(AdminServiceImpl adminService, UserServiceImpl userService, PicServiceImpl picService) {
        this.adminService = adminService;
        this.userService = userService;
        this.picService = picService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AdminDomain adminDomain = adminService.readUserByUsername(username);
        if (adminDomain == null) {
            UserDomain userDomain = userService.readUserByUsername(username);
            if (userDomain == null) {
                PicDomain picDomain = picService.readUserByUsername(username);
                if (picDomain == null) {
                    throw new UsernameNotFoundException(username);
                }
                return new org.springframework.security.core.userdetails.User(picDomain.getUsername(),
                        picDomain.getPassword(), getAuthorities(picDomain.getRole()));
            }
            return new org.springframework.security.core.userdetails.User(userDomain.getUsername(),
                    userDomain.getPassword(), getAuthorities(userDomain.getRole()));
        }
        return new org.springframework.security.core.userdetails.User(adminDomain.getUsername(),
                adminDomain.getPassword(), getAuthorities(adminDomain.getRole()));
    }

    private Collection<? extends GrantedAuthority> getAuthorities(String role) {
        return List.of(new SimpleGrantedAuthority(role));
    }
}
