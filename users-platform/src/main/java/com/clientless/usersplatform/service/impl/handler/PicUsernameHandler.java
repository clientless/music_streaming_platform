package com.clientless.usersplatform.service.impl.handler;

import com.clientless.usersplatform.domain.PicDomain;
import com.clientless.usersplatform.repository.PicRepository;
import org.springframework.web.bind.annotation.ControllerAdvice;

import java.util.List;
import java.util.Random;

@ControllerAdvice
public class PicUsernameHandler {
    private final PicRepository picRepository;

    public PicUsernameHandler(PicRepository picRepository) {
        this.picRepository = picRepository;
    }

    public boolean IsPicExistByEmail(String picEmail) {
        boolean isExist = false;
        List<PicDomain> picDomainList = picRepository.findAll();
        for (PicDomain picDomain : picDomainList) {
            if (picEmail.equals(picDomain.getEmail())) {
                isExist = true;
                break;
            }
        }
        return isExist;
    }

    public boolean isPicExistByUsername(String username) {
        PicDomain picDomainCheck = picRepository.findByUsername(username);
        return picDomainCheck != null;
    }

    public String generateUserName(String fullName) {
        String[] words = fullName.split("[^a-zA-Z0-9']+");
        String codePic = "PIC-MSP";
        String randomValue = Integer.toString(new Random().nextInt(10000));
        StringBuilder baseUsername = createBaseUsername(words, codePic);
        String generatedUsername = appendRandomValue(baseUsername, randomValue);
        return isPicExistByUsername(generatedUsername) ? generateUserName(fullName) : generatedUsername;

    }

    private StringBuilder createBaseUsername(String[] words, String codePic) {
        StringBuilder baseUsername = new StringBuilder();

        if (words.length > 0) {
            baseUsername.append(words[0]);
            for (int i = 1; i < words.length; i++) {
                if (!words[i].isEmpty()) {
                    baseUsername.append(words[i].charAt(0));
                }
            }
            baseUsername.append(codePic);
        }

        return baseUsername;
    }

    private String appendRandomValue(StringBuilder baseUsername, String randomValue) {
        return baseUsername.append(randomValue).toString();
    }

}
