package com.clientless.usersplatform.service.impl;

import com.clientless.usersplatform.domain.AdminDomain;
import com.clientless.usersplatform.domain.CodeVerifyAdminDomain;
import com.clientless.usersplatform.dto.admin.*;
import com.clientless.usersplatform.exception.ConflictCustomExceptionJWT;
import com.clientless.usersplatform.exception.NotFoundException;
import com.clientless.usersplatform.model.Response;
import com.clientless.usersplatform.projection.AdminCodeVerifyResponse;
import com.clientless.usersplatform.projection.AdminResetPasswordResponse;
import com.clientless.usersplatform.projection.AdminUpdateEmailOrPasswordResponse;
import com.clientless.usersplatform.repository.AdminRepository;
import com.clientless.usersplatform.repository.CodeVerifyAdminRepository;
import com.clientless.usersplatform.security.BCrypt;
import com.clientless.usersplatform.service.AdminService;
import com.clientless.usersplatform.service.impl.handler.AdminEmailCompanyHandler;
import com.clientless.usersplatform.service.impl.handler.AdminUsernameHandler;
import com.clientless.usersplatform.service.impl.handler.mimeMeesage.AdminMimeMessage;
import com.clientless.usersplatform.util.GenerateResponse;
import com.clientless.usersplatform.util.ValidationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class AdminServiceImpl implements AdminService {
    public static final Logger logger = LoggerFactory.getLogger(AdminServiceImpl.class);
    private final AdminRepository adminRepository;
    private final CodeVerifyAdminRepository codeVerifyAdminRepository;

    private final AdminUsernameHandler adminUsernameHandler;
    private final AdminEmailCompanyHandler adminEmailCompanyHandler;
    private final ValidationService validationService;
    private final JavaMailSender javaMailSender;
    private final HttpServletResponse httpServletResponse;

    AdminMimeMessage adminMimeMessage = new AdminMimeMessage();

    public AdminServiceImpl(AdminRepository adminRepository, CodeVerifyAdminRepository codeVerifyAdminRepository, AdminUsernameHandler adminUsernameHandler, AdminEmailCompanyHandler adminEmailCompanyHandler, ValidationService validationService, JavaMailSender javaMailSender, HttpServletResponse httpServletResponse) {
        this.adminRepository = adminRepository;
        this.codeVerifyAdminRepository = codeVerifyAdminRepository;
        this.adminUsernameHandler = adminUsernameHandler;
        this.adminEmailCompanyHandler = adminEmailCompanyHandler;
        this.validationService = validationService;
        this.javaMailSender = javaMailSender;
        this.httpServletResponse = httpServletResponse;
    }

    public AdminDomain readUserByUsername(String username) {
        return adminRepository.findByUsername(username);
    }

    @Transactional
    @Override
    public ResponseEntity<Response> register(AdminRegisterDto request) throws IOException, MessagingException {
        logger.info("Request to register: {}", request.getEmail());

        // validate email & password not blank & email-format
        validationService.validate(request);

        if (Objects.equals(request.getFullname(), "") || request.getFullname().split(" ").length == 0) {
            String msgErr = "Field Fullname Must be Filled";
            logger.error(msgErr);
            return GenerateResponse.badRequest("Bad Request", msgErr);
        }

        // check used email
        boolean isExistByEmail = adminUsernameHandler.IsAdminExistByEmail(request.getEmail());
        if (isExistByEmail) {
            logger.info("Checking email of: {}", request.getEmail());
            String errMsg = "Account with your email has been registered.";
            ConflictCustomExceptionJWT conflictCustomExceptionJWT = new ConflictCustomExceptionJWT();
            conflictCustomExceptionJWT.generateError(httpServletResponse, errMsg);
            return GenerateResponse.conflict("Conflict", errMsg);
        }
        AdminDomain adminDomain = new AdminDomain();

        String username = adminUsernameHandler.generateUserName(request.getFullname());
        String emailCompany = adminEmailCompanyHandler.generateEmailCompany(request.getFullname());
        adminDomain.setEmail(request.getEmail());
        adminDomain.setUsername(username);
        adminDomain.setFullname(request.getFullname());
        adminDomain.setEmailCompany(emailCompany);
        adminDomain.setPassword(BCrypt.hashpw(request.getPassword(), BCrypt.gensalt()));
//        adminDomain.setPassword(passwordEncoder.encode(request.getPassword()));
        adminDomain.setStatus(true);

        adminRepository.save(adminDomain);

        MimeMessage mimeMessage = adminMimeMessage.createMimeMessagesAdminRegistered(request, adminDomain, javaMailSender);

        javaMailSender.send(mimeMessage);

        return GenerateResponse.created("Registration success", adminDomain);
    }

    @Transactional
    @Override
    public ResponseEntity<Response> get(String username) throws IOException {
        logger.info("Load data of username: {}", username);

        AdminDomain adminDomain = adminRepository.findByUsername(username);
        if (adminDomain == null) {
            throw new NotFoundException(username);
        }

        return GenerateResponse.success("Data found", adminDomain);
    }

    @Override
    public ResponseEntity<Response> getList() throws IOException {
        try {
            List<AdminDomain> adminDomainList = new ArrayList<>(adminRepository.findAll());
            return GenerateResponse.success("Successfully get data", adminDomainList);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return GenerateResponse.error("Something Wrong", null);
        }
    }

    @Transactional
    @Override
    public ResponseEntity<Response> update(String username, AdminUpdateDto request) throws IOException {
        logger.info("Request to update: {}", username);

        // validate email in the right pattern
        validationService.validate(request);

        AdminDomain adminDomain = adminRepository.findByUsername(username);
        if (adminDomain == null) {
            throw new NotFoundException(username);
        }

        if (!request.getEmail().isBlank() || !request.getEmail().isEmpty()) {
            adminDomain.setEmail(request.getEmail());
        }
        if (!request.getFullname().isBlank() || !request.getFullname().isEmpty()) {
            adminDomain.setFullname(request.getFullname());
        }
        if (!request.getPassword().isBlank() || !request.getPassword().isEmpty()) {
            adminDomain.setPassword(BCrypt.hashpw(request.getPassword(), BCrypt.gensalt()));
        }
        if (!request.getEmailCompany().isBlank() || !request.getEmailCompany().isEmpty()) {
            adminDomain.setEmailCompany(request.getEmailCompany());
        }

        adminRepository.save(adminDomain);
        return GenerateResponse.success("Update success", adminDomain);
    }

    @Transactional
    @Override
    public ResponseEntity<Response> updateEmailOrPassword(String username, AdminUpdateEmailOrPasswordDto request) throws IOException, MessagingException {
        logger.info("Request to update: {}", username);

        // validate email in the right pattern
        validationService.validate(request);

        AdminDomain adminDomain = adminRepository.findByUsername(username);
        if (adminDomain == null) {
            return GenerateResponse.notFound("Not Found User With Username", username);
        } else {
            if (!request.getEmail().isBlank() || !request.getEmail().isEmpty()) {
                adminDomain.setEmail(request.getEmail());
                if (!request.getPassword().isBlank() || !request.getPassword().isEmpty()) {
                    adminDomain.setPassword(BCrypt.hashpw(request.getPassword(), BCrypt.gensalt()));
                    adminRepository.save(adminDomain);
                    MimeMessage message = adminMimeMessage.createMimeMessageAdminUpdateEmailOrPassword(request, adminDomain, javaMailSender);
                    javaMailSender.send(message);
                    AdminUpdateEmailOrPasswordResponse adminUpdateEmailOrPasswordResponse = new AdminUpdateEmailOrPasswordResponse(adminDomain.getEmail(), adminDomain.getPassword());
                    return GenerateResponse.success("Update success", adminUpdateEmailOrPasswordResponse);
                } else {
                    return GenerateResponse.badRequest("Password must be filled", null);
                }
            } else {
                return GenerateResponse.badRequest("Email must be filled", null);
            }
        }
    }


    @Transactional
    @Override
    public ResponseEntity<Response> delete(String username) throws IOException {
        logger.info("Request to delete: {}", username);

        AdminDomain adminDomain = adminRepository.findByUsername(username);
        if (adminDomain == null) {
            throw new NotFoundException(username);
        }

        adminDomain.setStatus(false);
        adminRepository.save(adminDomain);
        return GenerateResponse.success("Delete data success", adminDomain);
    }

    @Override
    public ResponseEntity<Response> resetPassword(AdminResetPasswordDto request) throws IOException, MessagingException {
        // validate email in the right pattern
        validationService.validate(request);
        AdminDomain adminDomain = adminRepository.findByUsernameOrEmailCompany("", request.getEmailCompany());
        if (adminDomain == null) {
            return GenerateResponse.notFound("Not Found User With Email Company", request.getEmailCompany());
        } else {
            logger.info("Request to update: {}", adminDomain.getUsername());
            if (!request.getPassword().isBlank() || !request.getPassword().isEmpty()) {
                adminDomain.setPassword(BCrypt.hashpw(request.getPassword(), BCrypt.gensalt()));
                adminRepository.save(adminDomain);
                MimeMessage message = adminMimeMessage.createMimeMessageAdminResetPassword(request, adminDomain, javaMailSender);
                javaMailSender.send(message);
                AdminResetPasswordResponse adminResetPasswordResponse = new AdminResetPasswordResponse(adminDomain.getId(), adminDomain.getEmailCompany(), adminDomain.getPassword());
                return GenerateResponse.success("Update success", adminResetPasswordResponse);
            } else {
                return GenerateResponse.badRequest("Password Must Be Filled", null);
            }
        }
    }

    @Override
    @Transactional
    public ResponseEntity<Response> verifyCode(AdminCodeVerifyDto request) throws IOException {
        logger.info("Request to update: {}", request.getCodeAccess());

        CodeVerifyAdminDomain codeVerifyAdminDomain = codeVerifyAdminRepository.findCodeVerifyAdminDomainByCodeAccessAndCodeAccessWasActivated(request.getCodeAccess(), false);
        if (codeVerifyAdminDomain == null) {
            AdminCodeVerifyResponse adminCodeVerifyResponse = new AdminCodeVerifyResponse("Failed");
            return GenerateResponse.notFound("Data Not Found with Code Access " + request.getCodeAccess(), adminCodeVerifyResponse);
        } else if ((!request.getCodeAccess().isBlank() || !request.getCodeAccess().isEmpty()) && !codeVerifyAdminDomain.isCodeAccessWasActivated()) {
            codeVerifyAdminDomain.setCodeAccessWasActivated(true);
            codeVerifyAdminRepository.save(codeVerifyAdminDomain);
            AdminCodeVerifyResponse adminCodeVerifyResponse = new AdminCodeVerifyResponse("SUCCESS");
            return GenerateResponse.success("Update success", adminCodeVerifyResponse);

        } else {
            AdminCodeVerifyResponse adminCodeVerifyResponse = new AdminCodeVerifyResponse("Failed");
            return GenerateResponse.badRequest("Update Failed Code Access Blank Or Empty", adminCodeVerifyResponse);
        }
    }
}
