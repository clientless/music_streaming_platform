package com.clientless.usersplatform.service;

import com.clientless.usersplatform.dto.pic.PicRegisterDto;
import com.clientless.usersplatform.dto.pic.PicResetPasswordDto;
import com.clientless.usersplatform.dto.pic.PicUpdateDto;
import com.clientless.usersplatform.dto.pic.PicUpdateEmailOrPasswordDto;
import com.clientless.usersplatform.model.Response;
import org.springframework.http.ResponseEntity;

import javax.mail.MessagingException;
import java.io.IOException;

public interface PicService {
    ResponseEntity<Response> register(PicRegisterDto request) throws IOException, MessagingException;

    ResponseEntity<Response> get(String username) throws IOException;
    ResponseEntity<Response> getList() throws IOException;


    ResponseEntity<Response> update(String username, PicUpdateDto request) throws IOException;

    ResponseEntity<Response> updateEmailOrPassword(String username, PicUpdateEmailOrPasswordDto request) throws IOException, MessagingException;

    ResponseEntity<Response> delete(String username) throws IOException;

    ResponseEntity<Response> resetPassword(PicResetPasswordDto request) throws IOException, MessagingException;

}
