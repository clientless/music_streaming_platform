package com.clientless.usersplatform.service.impl.handler;

import com.clientless.usersplatform.domain.AdminDomain;
import com.clientless.usersplatform.repository.AdminRepository;
import org.springframework.web.bind.annotation.ControllerAdvice;

import java.util.List;
import java.util.Random;

@ControllerAdvice
public class AdminEmailCompanyHandler {
    private final AdminRepository adminRepository;

    public AdminEmailCompanyHandler(AdminRepository adminRepository) {
        this.adminRepository = adminRepository;
    }

    public boolean isAdminExistByEmailCompany(String emailCompany) {
        boolean isExist = false;
        List<AdminDomain> adminDomainList = adminRepository.findAll();
        for (AdminDomain adminDomain : adminDomainList) {
            if (emailCompany.equalsIgnoreCase(adminDomain.getEmailCompany())) {
                isExist = true;
                break;
            }
        }
        return isExist;
    }

    public String generateEmailCompany(String fullName) {
        String[] words = fullName.split("[^a-zA-Z0-9']+");
        String domainAdmin = "@adm-msp.com";
        String randomValue = Integer.toString(new Random().nextInt(10000));
        StringBuilder baseEmailCompany = createBaseEmailCompany(words, domainAdmin);
        String generatedUsername = appendRandomValue(baseEmailCompany, randomValue);
        return isAdminExistByEmailCompany(generatedUsername) ? generateEmailCompany(fullName) : generatedUsername;

    }

    private StringBuilder createBaseEmailCompany(String[] words, String domainAdmin) {
        StringBuilder baseEmailCompany = new StringBuilder();

        if (words.length > 0) {
            if (words.length == 1) {
                baseEmailCompany.append(words[0]);
            } else if (words.length == 2) {
                baseEmailCompany.append(words[0]).append(".").append(words[1]);
            } else {
                baseEmailCompany.append(words[0]).append(".").append(words[1]);
            }
            for (int i = 2; i < words.length; i++) {
                if (!words[i].isEmpty()) {
                    baseEmailCompany.append(".").append(words[i].charAt(0));
                }
            }
            baseEmailCompany.append(domainAdmin);
        }

        return baseEmailCompany;
    }

    private String appendRandomValue(StringBuilder baseEmailCompany, String randomValue) {
        String[] valStr = baseEmailCompany.toString().split("@");

        return valStr[0] + "." + randomValue + "@" + valStr[1];
    }


}
