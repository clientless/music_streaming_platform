package com.clientless.usersplatform.service.impl;

import com.clientless.usersplatform.domain.PicDomain;
import com.clientless.usersplatform.domain.UserDomain;
import com.clientless.usersplatform.dto.user.*;
import com.clientless.usersplatform.exception.ConflictCustomExceptionJWT;
import com.clientless.usersplatform.exception.NotFoundException;
import com.clientless.usersplatform.model.Response;
import com.clientless.usersplatform.projection.UserUpdateEmailOrPasswordResponse;
import com.clientless.usersplatform.repository.UserRepository;
import com.clientless.usersplatform.security.BCrypt;
import com.clientless.usersplatform.service.UserService;
import com.clientless.usersplatform.service.impl.handler.UserUsernameHandler;
import com.clientless.usersplatform.service.impl.handler.mimeMeesage.UserMimeMessage;
import com.clientless.usersplatform.util.GenerateResponse;
import com.clientless.usersplatform.util.ValidationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class UserServiceImpl implements UserService {
    public static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
    private final UserRepository userRepository;
    private final UserUsernameHandler userUsernameHandler;
    private final ValidationService validationService;
    private final JavaMailSender javaMailSender;
    private final HttpServletResponse httpServletResponse;

    UserMimeMessage userMimeMessage = new UserMimeMessage();

    public UserServiceImpl(UserRepository userRepository, UserUsernameHandler userUsernameHandler, ValidationService validationService, JavaMailSender javaMailSender, HttpServletResponse httpServletResponse) {
        this.userRepository = userRepository;
        this.userUsernameHandler = userUsernameHandler;
        this.validationService = validationService;
        this.javaMailSender = javaMailSender;
        this.httpServletResponse = httpServletResponse;
    }

    public UserDomain readUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }


    @Override
    public ResponseEntity<Response> register(UserRegisterDto request) throws IOException, MessagingException {
        logger.info("Request to register: {}", request.getEmail());

        // validate email & password not blank & email-format
        validationService.validate(request);

        if (Objects.equals(request.getFullname(), "") || request.getFullname().split(" ").length == 0) {
            String msgErr = "Field Fullname Must be Filled";
            logger.error(msgErr);
            return GenerateResponse.badRequest("Bad Request", msgErr);
        }


        // check used email
        boolean isExistByEmail = userUsernameHandler.isUserExistByUsername(request.getEmail());
        if (isExistByEmail) {
            logger.info("Checking email of: {}", request.getEmail());
            String errMsg = "Account with your email has been registered.";
            ConflictCustomExceptionJWT conflictCustomExceptionJWT = new ConflictCustomExceptionJWT();
            conflictCustomExceptionJWT.generateError(httpServletResponse, errMsg);
            return GenerateResponse.conflict("Conflict",errMsg);
        }

        String username = userUsernameHandler.generateUserName(request.getFullname());


        UserDomain userDomain = new UserDomain();
        userDomain.setEmail(request.getEmail());
        userDomain.setUsername(username);
        userDomain.setPassword(BCrypt.hashpw(request.getPassword(), BCrypt.gensalt()));
//        userDomain.setPassword(passwordEncoder.encode(request.getPassword()));
        userDomain.setFullname(request.getFullname());
        userDomain.setStatus(true);

        userRepository.save(userDomain);
//        SimpleMailMessage message = getSimpleMailMessageUserRegistered(request, userDomain);
        MimeMessage message = userMimeMessage.createMimeMessagesUserRegistered(request, userDomain, javaMailSender);

        javaMailSender.send(message);
        return GenerateResponse.created("Registration success", userDomain);
    }


    @Override
    public ResponseEntity<Response> get(String username) throws IOException {
        logger.info("Load data of username: {}", username);

        UserDomain userDomain = userRepository.findByUsername(username);
        if (userDomain == null) {
            throw new NotFoundException(username);
        }

        return GenerateResponse.success("Data user found", userDomain);
    }

    @Override
    public ResponseEntity<Response> getList() throws IOException {
        try {
            List<UserDomain> userDomainList = new ArrayList<>(userRepository.findAll());
            return GenerateResponse.success("Successfully get data",userDomainList );
        } catch (Exception e) {
            logger.error(e.getMessage());
            return GenerateResponse.error("Something Wrong", null);
        }    }

    @Override
    public ResponseEntity<Response> update(String username, UserUpdateDto request) throws IOException {
        logger.info("Request to update: {}", username);

        // validate email pattern
        validationService.validate(request);

        UserDomain userDomain = userRepository.findByUsername(username);
        if (userDomain == null) {
            throw new NotFoundException(username);
        }

        if (!request.getEmail().isBlank() || !request.getEmail().isEmpty()) {
            userDomain.setEmail(request.getEmail());
        }
        if (!request.getFullname().isBlank() || !request.getFullname().isEmpty()) {
            userDomain.setFullname(request.getFullname());
        }
        if (!request.getPassword().isBlank() || !request.getPassword().isEmpty()) {
            userDomain.setPassword(BCrypt.hashpw(request.getPassword(), BCrypt.gensalt()));
        }

        userRepository.save(userDomain);
        return GenerateResponse.success("Update user data success", userDomain);
    }

    @Transactional
    @Override
    public ResponseEntity<Response> updateEmailOrPassword(String username, UserUpdateEmailOrPasswordDto request) throws IOException, MessagingException {
        logger.info("Request to update: {}", username);

        // validate email in the right pattern
        validationService.validate(request);
        UserDomain userDomain = userRepository.findByUsername(username);
        if (userDomain == null) {
            return GenerateResponse.notFound("Data Not Found with Username: ", username);
        } else {
            if (!request.getEmail().isBlank() || !request.getEmail().isEmpty()) {
                userDomain.setEmail(request.getEmail());
                if (!request.getPassword().isBlank() || !request.getPassword().isEmpty()) {
                    userDomain.setPassword(BCrypt.hashpw(request.getPassword(), BCrypt.gensalt()));
                    userRepository.save(userDomain);
                    MimeMessage message = userMimeMessage.createMimeMessageUserUpdateEmailOrPassword(request, userDomain, javaMailSender);
                    javaMailSender.send(message);
                    UserUpdateEmailOrPasswordResponse userUpdateEmailOrPasswordResponse = new UserUpdateEmailOrPasswordResponse(userDomain.getEmail(), userDomain.getPassword());
                    return GenerateResponse.success("Update success", userUpdateEmailOrPasswordResponse);
                } else {
                    return GenerateResponse.badRequest("Password Must be Filled", null);
                }
            } else {
                return GenerateResponse.badRequest("Email Must Be Filled", null);
            }
        }
    }

    @Override
    public ResponseEntity<Response> delete(String username) throws IOException {
        logger.info("Request to update: {}", username);

        UserDomain userDomain = userRepository.findByUsername(username);
        if (userDomain == null) {
            throw new NotFoundException(username);
        }

        userDomain.setStatus(false);
        userRepository.save(userDomain);

        return GenerateResponse.success("Delete user data success", userDomain);
    }

    @Override
    public ResponseEntity<Response> resetPassword(UserResetPasswordDto request) throws IOException, MessagingException {
        // validate email in the right pattern
        validationService.validate(request);
        UserDomain userDomain = userRepository.findByEmailAndStatus(request.getEmail(), true);
        if (userDomain == null) {
            return GenerateResponse.notFound("Not Found Dat with Email: ", request.getEmail());
        } else {
            if (!request.getPassword().isBlank() || !request.getPassword().isEmpty()) {
                logger.info("Request to update: {}", request.getEmail());
                userDomain.setPassword(BCrypt.hashpw(request.getPassword(), BCrypt.gensalt()));
                userRepository.save(userDomain);
                MimeMessage message = userMimeMessage.createMimeMessageUserResetPassword(request, userDomain, javaMailSender);
                javaMailSender.send(message);

                UserUpdateEmailOrPasswordResponse userUpdateEmailOrPasswordResponse = new UserUpdateEmailOrPasswordResponse(userDomain.getEmail(), userDomain.getPassword());
                return GenerateResponse.success("Update success", userUpdateEmailOrPasswordResponse);
            } else {
                return GenerateResponse.badRequest("Password Must Be Filled", null);
            }
        }
    }
}
