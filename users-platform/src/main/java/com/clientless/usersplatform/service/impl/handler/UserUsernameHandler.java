package com.clientless.usersplatform.service.impl.handler;

import com.clientless.usersplatform.domain.UserDomain;
import com.clientless.usersplatform.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;

import java.util.List;
import java.util.Random;

@ControllerAdvice
public class UserUsernameHandler {
    @Autowired
    private UserRepository userRepository;

    public boolean isUserExistByUsername(String username) {
        UserDomain userDomainCheck = userRepository.findByUsername(username);
        return userDomainCheck != null;
    }

    public String generateUserName(String fullName) {
        String[] words = fullName.split("[^a-zA-Z0-9']+");
        String codeUser = "USER-MSP";
        String randomValue = Integer.toString(new Random().nextInt(10000));
        StringBuilder baseUsername = createBaseUsername(words, codeUser);
        String generatedUsername = appendRandomValue(baseUsername, randomValue);
        return isUserExistByUsername(generatedUsername) ? generateUserName(fullName) : generatedUsername;

    }

    private StringBuilder createBaseUsername(String[] words, String codeUser) {
        StringBuilder baseUsername = new StringBuilder();

        if (words.length > 0) {
            baseUsername.append(words[0]);
            for (int i = 1; i < words.length; i++) {
                if (!words[i].isEmpty()) {
                    baseUsername.append(words[i].charAt(0));
                }
            }
            baseUsername.append(codeUser);
        }

        return baseUsername;
    }

    private String appendRandomValue(StringBuilder baseUsername, String randomValue) {
        return baseUsername.append(randomValue).toString();
    }

}
