package com.clientless.usersplatform.service;

import com.clientless.usersplatform.dto.admin.*;
import com.clientless.usersplatform.model.Response;
import org.springframework.http.ResponseEntity;

import javax.mail.MessagingException;
import java.io.IOException;

public interface AdminService {
    ResponseEntity<Response> register(AdminRegisterDto request) throws IOException, MessagingException;

    ResponseEntity<Response> get(String username) throws IOException;
    ResponseEntity<Response> getList() throws IOException;


    ResponseEntity<Response> update(String username, AdminUpdateDto request) throws IOException;

    ResponseEntity<Response> updateEmailOrPassword(String username, AdminUpdateEmailOrPasswordDto request) throws IOException, MessagingException;

    ResponseEntity<Response> delete(String username) throws IOException;

    ResponseEntity<Response> resetPassword(AdminResetPasswordDto request) throws IOException, MessagingException;

    ResponseEntity<Response> verifyCode(AdminCodeVerifyDto request) throws IOException, MessagingException;


}
