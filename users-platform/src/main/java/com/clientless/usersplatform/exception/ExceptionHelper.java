package com.clientless.usersplatform.exception;

import com.clientless.usersplatform.model.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.ConstraintViolationException;

@ControllerAdvice
public class ExceptionHelper {
    private static final Logger logger = LoggerFactory.getLogger(ExceptionHelper.class);

    @ExceptionHandler(value = {HttpClientErrorException.Unauthorized.class})
    public ResponseEntity<Response> handleUnauthorizedException(HttpClientErrorException.Unauthorized ex) {
        logger.error("Exception: ", ex.getMessage());
        return new ResponseEntity<>(new Response(ex.getMessage(), null, HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {HttpClientErrorException.Forbidden.class})
    public ResponseEntity<Response> handleForbiddenException(HttpClientErrorException.Unauthorized ex) {
        logger.error("Exception: ", ex.getMessage());
        return new ResponseEntity<>(new Response(ex.getMessage(), null, HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {NotFoundException.class})
    public ResponseEntity<Response> handleNotFoundException(NotFoundException ex) {
        logger.error("Not Found Exception: ", ex.getMessage());
        return new ResponseEntity<>(new Response(ex.getMessage(), null, HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {MissingServletRequestParameterException.class})
    public ResponseEntity<Response> handleBadRequestException(MissingServletRequestParameterException ex) {
        logger.error("Bad Request Exception: ", ex.getMessage());
        return new ResponseEntity<>(new Response(ex.getMessage(), null, HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Response> constraintViolationException(ConstraintViolationException ex) {
        logger.error("Validation Exception: ", ex.getMessage());
//        return GenerateResponse.badRequest(ex.getMessage(), null);
        return new ResponseEntity<>(new Response(ex.getMessage(), null, HttpStatus.BAD_REQUEST.value()),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {ResponseStatusException.class})
    public ResponseEntity<Response> handleApiException(ResponseStatusException ex) {
        logger.error("Exception: ", ex.getMessage());
        return new ResponseEntity<>(new Response(ex.getMessage(), null, ex.getRawStatusCode()),
                HttpStatus.valueOf(ex.getRawStatusCode()));
    }
}
