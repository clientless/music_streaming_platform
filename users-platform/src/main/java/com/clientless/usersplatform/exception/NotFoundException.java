package com.clientless.usersplatform.exception;

public class NotFoundException extends RuntimeException {

    public NotFoundException(String request) {
        super(request + " not found");
    }
}
