package com.clientless.usersplatform.exception;

import com.clientless.usersplatform.model.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UnauthorizedCustomExceptionJWT {
    private static final Logger logger = LoggerFactory.getLogger(UnauthorizedCustomExceptionJWT.class);

    public void generateError(HttpServletResponse response, String msgErr) throws IOException {
        Response objResponse = new Response("Unauthorized", msgErr, HttpStatus.UNAUTHORIZED.value());
        String returnToString = objResponse.toString();
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED); // Atau status HTTP yang sesuai
        response.setContentType("application/json");
        response.getWriter().write(returnToString);
        response.getWriter().flush();
        response.getWriter().close();
        logger.error(returnToString);
    }


}
