package com.clientless.usersplatform.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "admin_data")
public class AdminDomain {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String email;

    private String username;

    private String fullname;

    @Column(name = "email_company")
    private String emailCompany;

    //    @JsonIgnore
    private String password;

    private boolean status;
    private String role = "ADMIN"; // Memberikan nilai default langsung
}
