package com.clientless.usersplatform.config;

public class AuthenticationConfigConstants {
    public static final String SECRET = "Msp_Climis";
    public static final long EXPIRATION_TIME = 7200000; // 5 min
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String AUTH_URL_ADMIN = "/v1/auth/admin/**";
    public static final String AUTH_URL_PIC = "/v1/auth/pic/**";
    public static final String AUTH_URL_USER = "/v1/auth/user/**";

}
