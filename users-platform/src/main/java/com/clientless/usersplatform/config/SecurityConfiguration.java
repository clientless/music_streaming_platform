package com.clientless.usersplatform.config;

import com.clientless.usersplatform.filter.JWTAuthenticationFilterLogin;
import com.clientless.usersplatform.filter.JWTAuthorizationFilter;
import com.clientless.usersplatform.repository.AdminRepository;
import com.clientless.usersplatform.repository.PicRepository;
import com.clientless.usersplatform.repository.UserRepository;
import com.clientless.usersplatform.service.impl.AuthenticationUserDetailService;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final AuthenticationUserDetailService authenticationUserDetailService;
    private final AdminRepository adminRepository;
    private final PicRepository picRepository;
    private final UserRepository userRepository;


    public SecurityConfiguration(BCryptPasswordEncoder bCryptPasswordEncoder, AuthenticationUserDetailService authenticationUserDetailService, AdminRepository adminRepository, PicRepository picRepository, UserRepository userRepository) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.authenticationUserDetailService = authenticationUserDetailService;
        this.adminRepository = adminRepository;
        this.picRepository = picRepository;
        this.userRepository = userRepository;
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable().authorizeRequests()
                .antMatchers(AuthenticationConfigConstants.AUTH_URL_ADMIN).permitAll()
                .antMatchers(AuthenticationConfigConstants.AUTH_URL_PIC).permitAll()
                .antMatchers(AuthenticationConfigConstants.AUTH_URL_USER).permitAll()
                .antMatchers(HttpMethod.GET, "/v1/tc/**").permitAll()
                //ROLE BASED AUTHENTICATION START
                .antMatchers("/v1/admin/**").hasAnyAuthority("ADMIN")
                .antMatchers("/v1/pic/**").hasAnyAuthority("PIC", "ADMIN")
                .antMatchers("/v1/user/**").hasAnyAuthority("USER", "ADMIN")
                //ROLE BASED AUTHENTICATION END
                .anyRequest().authenticated()
                .and()
                .addFilter(new JWTAuthenticationFilterLogin(authenticationManager(), adminRepository, picRepository, userRepository))
                .addFilter(new JWTAuthorizationFilter(authenticationManager()))
                // this disables session creation on Spring Security
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(authenticationUserDetailService).passwordEncoder(bCryptPasswordEncoder);

    }
}
