document.addEventListener('DOMContentLoaded', function() {
    var auth = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJMdWtpdG9BQURNLU1TUDE1OTYiLCJyb2xlIjoiQURNSU4iLCJleHAiOjE3MTI4NjQxNTl9.da03thPVlFPmXpNSyv955sjKL89iIuipD7AfBaJvRZAP6E7Mt_3MJnyTys_UjXGeqrucHbLwCe69yq9eX_AvfQ';
    var audioPlayer = playAudioFromURL('http://localhost:8081/v1/song/play_song/MSPSONG5', auth);

    var playBtn = document.getElementById('play-btn');
    var pauseBtn = document.getElementById('pause-btn');
    var stopBtn = document.getElementById('stop-btn');

    playBtn.addEventListener('click', function() {
        audioPlayer.play();
    });

    pauseBtn.addEventListener('click', function() {
        audioPlayer.pause();
    });

    stopBtn.addEventListener('click', function() {
        audioPlayer.stop();
    });
});

function playAudioFromURL(url, auth) {
    var audio = document.getElementById('my-audio');
    var lastPausedTime = 0; // Menyimpan waktu terakhir ketika audio dijeda

    function play() {
        if (lastPausedTime > 0) {
            audio.currentTime = lastPausedTime;
            lastPausedTime = 0; // Mengatur kembali waktu terakhir ke 0 setelah dimulai kembali
        }
        audio.play();
    }

    function pause() {
        if (audio.paused) {
            audio.play();
        } else {
            audio.pause();
            lastPausedTime = audio.currentTime; // Menyimpan waktu saat ini dalam detik
        }
    }

    function stop() {
        audio.pause();
        audio.currentTime = 0;
        lastPausedTime = 0; // Mengatur kembali waktu terakhir ke 0 setelah dihentikan
    }

    var headers = new Headers();
    headers.append('Authorization', auth); // Menambahkan header otorisasi

    fetch(url, {
            headers: headers,
            mode: 'cors' // Menggunakan mode 'cors' untuk permintaan melalui CORS
        })
        .then(response => response.arrayBuffer())
        .then(dataByte => {
            var byteArray = new Uint8Array(dataByte);
            var blob = new Blob([byteArray], {
                type: 'audio/mpeg'
            });
            var audioUrl = URL.createObjectURL(blob);

            audio.src = audioUrl;
        })
        .catch(error => {
            console.error('Error:', error);
        });

    return {
        play: play,
        pause: pause,
        stop: stop
    };
}
