document.addEventListener('DOMContentLoaded', function() {
    var audioPlayer = playAudioFromURL('http://127.0.0.1:8081/v1/song/play_song/MSPSONG5');

    var playBtn = document.getElementById('play-btn');
    var pauseBtn = document.getElementById('pause-btn');
    var stopBtn = document.getElementById('stop-btn');

    playBtn.addEventListener('click', function() {
        audioPlayer.play();
    });

    pauseBtn.addEventListener('click', function() {
        audioPlayer.pause();
    });

    stopBtn.addEventListener('click', function() {
        audioPlayer.stop();
    });
});

function playAudioFromURL(url) {
    var audio = document.getElementById('my-audio');
    var lastPausedTime = 0; // Menyimpan waktu terakhir ketika audio dijeda

    function play() {
        if (lastPausedTime > 0) {
            audio.currentTime = lastPausedTime;
            lastPausedTime = 0; // Mengatur kembali waktu terakhir ke 0 setelah dimulai kembali
        }
        audio.play();
    }

    function pause() {
        if (audio.paused) {
            audio.play();
        } else {
            audio.pause();
            lastPausedTime = audio.currentTime; // Menyimpan waktu saat ini dalam detik
        }
    }

    function stop() {
        audio.pause();
        audio.currentTime = 0;
        lastPausedTime = 0; // Mengatur kembali waktu terakhir ke 0 setelah dihentikan
    }

    fetch(url)
        .then(response => response.arrayBuffer())
        .then(dataByte => {
            var byteArray = new Uint8Array(dataByte);
            var blob = new Blob([byteArray], { type: 'audio/mpeg' });
            var audioUrl = URL.createObjectURL(blob);

            audio.src = audioUrl;
        })
        .catch(error => {
            console.error('Error:', error);
        });

    return {
        play: play,
        pause: pause,
        stop: stop
    };
}
